#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include <Mapi.h>
#include <axl.h>

#include "handler_osm.h"
#include "handler_osm_db.h"
#include "handler_osm_put.h"
#include "handler_osm_sql.h"
#include "handler_osm_delete.h"

static ret_t result_node_last_id(cherokee_handler_osm_t *hdl, MapiHdl *hdl1, cherokee_buffer_t *buf) {
        cherokee_buffer_add_va (buf, "%d", mapi_get_last_id(*hdl1));
        return ret_ok;
}

static ret_t node_update_or_insert (cherokee_handler_osm_t *hdl, MapiHdl *hdl1, cherokee_buffer_t *buf) {
        if (mapi_rows_affected(*hdl1) == 0)
                return ret_error;

        return ret_ok;
}

static void do_members(cherokee_handler_osm_t *hdl, axlNode *node, unsigned long int relationid, unsigned short int update) {
	axlNode * member;
	if ((member = axl_node_get_first_child(node)) != NULL) {
		cherokee_buffer_t members_node = CHEROKEE_BUF_INIT;
		cherokee_buffer_t members_way = CHEROKEE_BUF_INIT;
		cherokee_buffer_t members_relation = CHEROKEE_BUF_INIT;
		unsigned long int idx = 0;
		do {
			if (NODE_CMP_NAME (member, "member") &&
			    axl_node_has_attribute(member, "type") &&
			    axl_node_has_attribute(member, "ref") &&
			    axl_node_has_attribute(member, "role")) {

				unsigned long int ref = strtod(axl_node_get_attribute_value(member, "ref"), NULL);
				if (errno != ERANGE && ref > 0) {
					const char *type = axl_node_get_attribute_value(member, "type");
					const char *role = axl_node_get_attribute_value(member, "role");

					if (strcmp(type, "node") == 0) {
						cherokee_buffer_add_va (&members_node, "%lu, ", ref);
						cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
						if (update == 1) {
							cherokee_buffer_add_va (&sql1, "UPDATE relation_members_node SET to_node = %lu, role = '%s' WHERE relation = %lu AND idx = %lu", ref, role, relationid, idx);
						}
						if (update == 0 || run_sql(hdl, &sql1, NULL, node_update_or_insert) == ret_error) {
							cherokee_buffer_mrproper(&sql1);
							cherokee_buffer_add_va (&sql1, "INSERT INTO relation_members_node (relation, idx, to_node, role) VALUES (%lu, %lu, %lu, '%s')", relationid, idx, ref, role);
							run_sql(hdl, &sql1, NULL, NULL);
						}
						cherokee_buffer_mrproper(&sql1);
					} else if (strcmp(type, "way") == 0) {
						cherokee_buffer_add_va (&members_way, "%lu, ", ref);
						cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
						if (update == 1) {
							cherokee_buffer_add_va (&sql1, "UPDATE relation_members_way SET to_way = %lu, role = '%s' WHERE relation = %lu AND idx = %lu", ref, role, relationid, idx);
						}
						if (update == 0 || run_sql(hdl, &sql1, NULL, node_update_or_insert) == ret_error) {
							cherokee_buffer_mrproper(&sql1);
							cherokee_buffer_add_va (&sql1, "INSERT INTO relation_members_way (relation, idx, to_way, role) VALUES (%lu, %lu, %lu, '%s')", relationid, idx, ref, role);
							run_sql(hdl, &sql1, NULL, NULL);
						}
						cherokee_buffer_mrproper(&sql1);
					} else if (strcmp(type, "relation") == 0) {
						cherokee_buffer_add_va (&members_relation, "%lu, ", ref);
						cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
						if (update == 1) {
							cherokee_buffer_add_va (&sql1, "UPDATE relation_members_relation SET to_relation = %lu, role = '%s' WHERE relation = %lu AND idx = %lu", ref, role, relationid, idx);
						}
						if (update == 0 || run_sql(hdl, &sql1, NULL, node_update_or_insert) == ret_error) {
							cherokee_buffer_mrproper(&sql1);
							cherokee_buffer_add_va (&sql1, "INSERT INTO relation_members_relation (relation, idx, to_relation, role) VALUES (%lu, %lu, %lu, '%s')", relationid, idx, ref, role);
							run_sql(hdl, &sql1, NULL, NULL);
						}
						cherokee_buffer_mrproper(&sql1);
					} else {
						TRACE("osm", "%s: New type?\n", type);
					}
					idx++;
				}
			}
		} while ((member = axl_node_get_next(member)) != NULL);

		if (members_node.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&members_node, 2);
			cherokee_buffer_add_va (&sql1, "DELETE FROM relation_members_node WHERE relation = %lu AND to_node NOT IN (%s)", relationid, members_node.buf);
			run_sql(hdl, &sql1, NULL, NULL);
			cherokee_buffer_mrproper(&sql1);
		}			
		cherokee_buffer_mrproper(&members_node);
		
		if (members_way.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&members_way, 2);
			cherokee_buffer_add_va (&sql1, "DELETE FROM relation_members_way WHERE relation = %lu AND to_way NOT IN (%s)", relationid, members_way.buf);
			run_sql(hdl, &sql1, NULL, NULL);
			cherokee_buffer_mrproper(&sql1);
		}
		cherokee_buffer_mrproper(&members_way);

		if (members_relation.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&members_relation, 2);
			cherokee_buffer_add_va (&sql1, "DELETE FROM relation_members_relation WHERE relation = %lu AND to_relation NOT IN (%s)", relationid, members_relation.buf);
			run_sql(hdl, &sql1, NULL, NULL);
			cherokee_buffer_mrproper(&sql1);
		}
		cherokee_buffer_mrproper(&members_relation);
	}
}



static void do_nds(cherokee_handler_osm_t *hdl, axlNode *node, unsigned long int wayid, unsigned short int update) {
	axlNode * nd;
	if ((nd = axl_node_get_first_child(node)) != NULL) {
		cherokee_buffer_t nds = CHEROKEE_BUF_INIT;
		unsigned long int idx = 0;
		do {
			if (NODE_CMP_NAME (nd, "nd") &&
			    axl_node_has_attribute(nd, "ref")) {

				unsigned long int ref = strtod(axl_node_get_attribute_value(nd, "ref"), NULL);
				if (errno != ERANGE && ref > 0) {
					cherokee_buffer_add_va (&nds, "%lu, ", ref);

					cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
					if (update == 1) {
						cherokee_buffer_add_va (&sql1, "UPDATE way_nds SET to_node = %lu WHERE way = %lu AND idx = %lu", ref, wayid, idx);
					}
					if (update == 0 || run_sql(hdl, &sql1, NULL, node_update_or_insert) == ret_error) {
						cherokee_buffer_mrproper(&sql1);
						cherokee_buffer_add_va (&sql1, "INSERT INTO way_nds (way, idx, to_node) VALUES (%lu, %lu, %lu)", wayid, idx, ref);
						run_sql(hdl, &sql1, NULL, NULL);
					}
					cherokee_buffer_mrproper(&sql1);
					idx++;
				}
			}
		} while ((nd = axl_node_get_next(nd)) != NULL);

		if (nds.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&nds, 2);
			cherokee_buffer_add_va (&sql1, "DELETE FROM way_nds WHERE relation = %lu AND to_node NOT IN (%s)", wayid, nds.buf);
			run_sql(hdl, &sql1, NULL, NULL);
			cherokee_buffer_mrproper(&sql1);
		}
			
		cherokee_buffer_mrproper(&nds);
	}
}

static void do_tags(cherokee_handler_osm_t *hdl, axlNode *node, unsigned long int nodeid, unsigned short int update, char *create_sql, char *update_sql, char *delete_sql) {
	axlNode * tag;
	if ((tag = axl_node_get_first_child(node)) != NULL) {
		cherokee_buffer_t keys = CHEROKEE_BUF_INIT;
		do {
			if (NODE_CMP_NAME (tag, "tag") &&
			    axl_node_has_attribute(tag, "k") &&
			    axl_node_has_attribute(tag, "v")) {
				const char * key   = axl_node_get_attribute_value(tag, "k");
				size_t keylen = strlen(key);

				if (keylen > 0 && keylen < 256) {
					const char * value = axl_node_get_attribute_value(tag, "v");
					size_t valuelen = strlen(value);
					cherokee_buffer_add_va (&keys, "\'%s\', ", key);

					if (valuelen > 0 && valuelen < 256) {
						cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
						if (update == 1) {
							cherokee_buffer_add_va (&sql1, update_sql,
										       value,
					    					       nodeid,
										       key
									       );
						}
				    		if (update == 0 || run_sql(hdl, &sql1, NULL, node_update_or_insert) == ret_error) {
							cherokee_buffer_mrproper(&sql1);
							cherokee_buffer_add_va (&sql1, create_sql,
					    					       nodeid,
										       key,
										       value);
					    		run_sql(hdl, &sql1, NULL, NULL);
						}
						cherokee_buffer_mrproper(&sql1);
					}
				}
			}
		} while ((tag = axl_node_get_next(tag)) != NULL);

		if (keys.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&keys, 2);
			cherokee_buffer_add_va (&sql1, delete_sql, nodeid, keys.buf);
			run_sql(hdl, &sql1, NULL, NULL);
			cherokee_buffer_mrproper(&sql1);
		}
			
		cherokee_buffer_mrproper(&keys);
	}
}

static int
node_exists(cherokee_handler_osm_t *hdl, osm_state_delete_t type, unsigned long int nodeid) {
	int result = 0;
	cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
	cherokee_buffer_t tmp = CHEROKEE_BUF_INIT;

	switch (type) {
		case OSM_DELETE_NODE_COMMAND:
			cherokee_buffer_add_va (&sql1, SQL_NODE_EXIST, nodeid);
			break;

		case OSM_DELETE_WAY_COMMAND:
			cherokee_buffer_add_va (&sql1, SQL_WAY_EXIST, nodeid);
			break;

		case OSM_DELETE_RELATION_COMMAND:
			cherokee_buffer_add_va (&sql1, SQL_RELATION_EXIST, nodeid);
			break;

		default:
			SHOULDNT_HAPPEN;
			return result;
	}

	run_sql(hdl, &sql1, &tmp, NULL);
	if (tmp.len > 0)
		result = 1;
	
	cherokee_buffer_mrproper(&sql1);
	cherokee_buffer_mrproper(&tmp);

	return result;
}

static ret_t
create_node(cherokee_handler_osm_t *hdl, axlNode * node, osm_state_delete_t type, unsigned long int nodeid, const char *username, short int timestamp) {
	cherokee_connection_t   *conn = HANDLER_CONN(hdl);
	cherokee_buffer_t       *buf = &hdl->buffer;

	switch (type) {
		case OSM_DELETE_NODE_COMMAND: {
		if (axl_node_has_attribute(node, "lat") &&
		    axl_node_has_attribute(node, "lon")) {
		    	double lat, lon;
			lat = strtod(axl_node_get_attribute_value(node, "lat"), NULL);
			if (errno == ERANGE) {
				conn->error_code = http_bad_request;
				return ret_error;
			}

			lon = strtod(axl_node_get_attribute_value(node, "lon"), NULL);
			if (errno == ERANGE) {
				conn->error_code = http_bad_request;
				return ret_error;
			} else {
				/* TODO: more error checking */
				long testid = 0;	
	    	        	cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;

				if (nodeid == 0) {
					if (timestamp == 1 && axl_node_has_attribute(node, "timestamp")) {
						cherokee_buffer_add_va (&sql1, SQL_NODE_CREATE, lon, lat, username, "'", axl_node_get_attribute_value(node, "timestamp"), "'");
					} else {
						cherokee_buffer_add_va (&sql1, SQL_NODE_CREATE, lon, lat, username, SQL_NOW);
					}
				} else {
					if (timestamp == 1 && axl_node_has_attribute(node, "timestamp")) {
						cherokee_buffer_add_va (&sql1, SQL_NODE_CREATE_BY_ID, lon, lat, username, "'", axl_node_get_attribute_value(node, "timestamp"), "'", nodeid);
					} else {
						cherokee_buffer_add_va (&sql1, SQL_NODE_CREATE_BY_ID, lon, lat, username, SQL_NOW, nodeid);
					}
				}
				run_sql(hdl, &sql1, buf, result_node_last_id);
				cherokee_buffer_mrproper(&sql1);

				testid = strtol(buf->buf, (char **) NULL, 10);
				if (errno != ERANGE && testid > 0) {
					do_tags(hdl, node, testid, 0, SQL_NODE_CREATE_NODE_TAG, SQL_NODE_UPDATE_NODE_TAG, SQL_NODE_DELETE_NODE_TAG);
					return ret_ok;
				} else {
					return ret_error;
				}
			}
		}
	} 
	break;
		case OSM_DELETE_WAY_COMMAND: {
	   	cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
		cherokee_buffer_add_va (&sql1, SQL_WAY_CREATE, username);
		run_sql(hdl, &sql1, buf, result_node_last_id);
		cherokee_buffer_mrproper(&sql1);

		long testid = 0;
		testid = strtol(buf->buf, (char **) NULL, 10);
		if (errno != ERANGE && testid > 0) {
			do_tags(hdl, node, testid, 0, SQL_WAY_CREATE_NODE_TAG, SQL_WAY_UPDATE_NODE_TAG, SQL_WAY_DELETE_NODE_TAG);
			do_nds(hdl, node, testid, 0); /* this sequence otherwise we screw up type= */

			return ret_ok;
		} else {
			return ret_error;
		}
	} 
	break;
	case OSM_DELETE_RELATION_COMMAND: {
		cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
		cherokee_buffer_add_va (&sql1, SQL_RELATION_CREATE, username);
		run_sql(hdl, &sql1, buf, result_node_last_id);
		cherokee_buffer_mrproper(&sql1);

		long testid = 0;
		testid = strtol(buf->buf, (char **) NULL, 10);
		if (errno != ERANGE && testid > 0) {
			do_members(hdl, node, testid, 0);
			do_tags(hdl, node, testid, 0, SQL_RELATION_CREATE_NODE_TAG, SQL_RELATION_UPDATE_NODE_TAG, SQL_RELATION_DELETE_NODE_TAG);

			return ret_ok;
		} else {
			return ret_error;
		}
	}
	break;
	default:
		SHOULDNT_HAPPEN;
	}
	SHOULDNT_HAPPEN;
	return ret_error;
}

static ret_t
update_node(cherokee_handler_osm_t *hdl, axlNode * node, osm_state_delete_t type, unsigned long int nodeid, const char *username, short int timestamp) {
	cherokee_connection_t   *conn = HANDLER_CONN(hdl);

	if (axl_node_has_attribute_value(node, "visible", "false")) {
		/* the user wants to delete stuff by updating, that is nice
		 * but we will not update. Instead we do what the user request:
		 * deletion.
		 */
		return delete_object_by_id(hdl, nodeid, type);
	} else {
		switch (type) {
			case OSM_DELETE_NODE_COMMAND: {
			if (axl_node_has_attribute(node, "lat") &&
			    axl_node_has_attribute(node, "lon")) {
			    	double lat, lon;
				lat = strtod(axl_node_get_attribute_value(node, "lat"), NULL);
				if (errno == ERANGE) {
					conn->error_code = http_bad_request;
					return ret_error;
				}
					
				lon = strtod(axl_node_get_attribute_value(node, "lon"), NULL);
				if (errno == ERANGE) {
					conn->error_code = http_bad_request;
					return ret_error;
				} else {
			    		cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
					if (timestamp == 1 && axl_node_has_attribute(node, "timestamp")) {
						cherokee_buffer_add_va (&sql1, SQL_NODE_UPDATE_BY_ID, lon, lat, username,
						                               "'", axl_node_get_attribute_value(node, "timestamp"), "'", nodeid);
					} else {
						cherokee_buffer_add_va (&sql1, SQL_NODE_UPDATE_BY_ID, lon, lat, username, SQL_NOW, nodeid);
					}
					run_sql(hdl, &sql1, NULL, NULL);
					cherokee_buffer_mrproper(&sql1);

					do_tags(hdl, node, nodeid, 1, SQL_NODE_CREATE_NODE_TAG, SQL_NODE_UPDATE_NODE_TAG, SQL_NODE_DELETE_NODE_TAG);

					return ret_ok;
				}
			}
			}
			break;
			case OSM_DELETE_WAY_COMMAND: {	
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			if (timestamp == 1 && axl_node_has_attribute(node, "timestamp")) {
				cherokee_buffer_add_va (&sql1, SQL_WAY_UPDATE, username, 
							       "'", axl_node_get_attribute_value(node, "timestamp"), "'", nodeid);
			} else {
				cherokee_buffer_add_va (&sql1, SQL_WAY_UPDATE, username, SQL_NOW, nodeid);
			}
			run_sql(hdl, &sql1, NULL, NULL);
			cherokee_buffer_mrproper(&sql1);

			do_tags(hdl, node, nodeid, 1, SQL_WAY_CREATE_NODE_TAG, SQL_WAY_UPDATE_NODE_TAG, SQL_WAY_DELETE_NODE_TAG);
			do_nds(hdl, node, nodeid, 1); /* this sequence otherwise we screw up type = */

			return ret_ok;
			}
			break;
			case OSM_DELETE_RELATION_COMMAND: {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			if (timestamp == 1 && axl_node_has_attribute(node, "timestamp")) {
				cherokee_buffer_add_va (&sql1, SQL_RELATION_UPDATE, username,
							       "'", axl_node_get_attribute_value(node, "timestamp"), "'", nodeid);
			} else {
				cherokee_buffer_add_va (&sql1, SQL_RELATION_UPDATE, username, SQL_NOW, nodeid);
			}
			run_sql(hdl, &sql1, NULL, NULL);
			cherokee_buffer_mrproper(&sql1);

			do_members(hdl, node, nodeid, 1);
			do_tags(hdl, node, nodeid, 1, SQL_RELATION_CREATE_NODE_TAG, SQL_RELATION_UPDATE_NODE_TAG, SQL_RELATION_DELETE_NODE_TAG);

			return ret_ok;
			}
			break;
			default:
				SHOULDNT_HAPPEN;
		}

	}

	SHOULDNT_HAPPEN;
	return ret_error;
}



ret_t 
cherokee_handler_osm_init_put (cherokee_handler_osm_t *hdl)
{
        off_t                   postl;
        ret_t                    ret  = ret_ok;
        cherokee_buffer_t        post = CHEROKEE_BUF_INIT;
        cherokee_connection_t   *conn = HANDLER_CONN(hdl);
	axlDoc   * doc;
	axlError * error;

        /* Check for the post info
         */
        cherokee_post_get_len (&conn->post, &postl);
        if (postl <= 0 || postl >= (INT_MAX-1)) {
                conn->error_code = http_bad_request;
                return ret_error;
        }


	/* Process line per line
         */
        cherokee_post_walk_read (&conn->post, &post, (cuint_t) postl);

	TRACE("post", "%s\n", post.buf);

	doc = axl_doc_parse (post.buf, post.len, &error);

	if (doc == NULL) {
		printf ("Error found: %s\n", axl_error_get (error));
		axl_error_free (error);
		ret = ret_error;
	} else {
		int isroot = 0;
		axlNode * node = axl_doc_get_root (doc);
		node = axl_node_get_first_child (node);
		ret = ret_ok;
		
		/* het mooiste zou zijn om het onderstaande in twee functies te zetten
		 * update vs create.
		 * Als dat gebeurd is kan hier (of in de 'update' code gechecked worden
		 * op 'root' of op normale user en aan de hand daarvan weer switchen op
		 * empty == create of full == update)
		 */
		cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
		cherokee_buffer_add_str (&sql1, SQL_TRANSACTION_START);
		run_sql(hdl, &sql1, &hdl->buffer, NULL);
		cherokee_buffer_mrproper(&sql1);

		isroot = (cherokee_buffer_cmp_str(conn->realm_ref,"root") == 0);

		while (node != NULL && ret == ret_ok) {
			const char *username = NULL;
			unsigned long int nodeid = 0;
			osm_state_delete_t type;

			if (axl_node_has_attribute(node, "id")) {
				const char *id = axl_node_get_attribute_value(node, "id");
				nodeid = strtoul(id, (char **) NULL, 10);
				TRACE("osm", "%lu\n", nodeid);
			}

			if (isroot) {
				username = axl_node_get_attribute_value(node, "user");
			} else {
				username =  conn->realm_ref->buf;
			}
	                
			if (NODE_CMP_NAME (node, "node")) {
        	                type = OSM_DELETE_NODE_COMMAND;
	                } else if (NODE_CMP_NAME (node, "way")) {
        	                type = OSM_DELETE_WAY_COMMAND;
	                } else if (NODE_CMP_NAME (node, "relation")) {
				type = OSM_DELETE_RELATION_COMMAND;
        	        } else {
				ret = ret_error;
				break;
			}

			if (nodeid > 0 && node_exists(hdl, type, nodeid)) {
				/* UPDATE */
				ret = update_node(hdl, node, type, nodeid, username, isroot);
			} else {
				/* CREATE */
				ret = create_node(hdl, node, type, nodeid, username, isroot);
			}

			node = axl_node_get_next(node);
		}

		if (ret != ret_ok) {
			cherokee_buffer_add_str (&sql1, SQL_TRANSACTION_ROLLBACK);
			run_sql(hdl, &sql1, &hdl->buffer, NULL);
			cherokee_buffer_mrproper(&sql1);
		} else {
			cherokee_buffer_add_str (&sql1, SQL_TRANSACTION_COMMIT);
			run_sql(hdl, &sql1, &hdl->buffer, NULL);
			cherokee_buffer_mrproper(&sql1);
		}

		axl_doc_free (doc);
	}
        
	cherokee_buffer_mrproper (&post);
        return ret;
}


