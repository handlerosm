#!/bin/sh
mkdir -p /tmp/import
cd /tmp/import
wget -O planet-nl-latest.osm.gz http://hypercube.telascience.org/planet/planet-nl-latest.osm.gz
gzip -d planet-nl-latest.osm.gz
/opt/cherokee/bin/osmparser planet-nl-latest.osm > import.sql

ls -1 *.csv | while read i; do
	sort -u $i > $i-sorted
	mv $i-sorted $i
done

/opt/monetdb/bin/mclient -lsql < import.sql
