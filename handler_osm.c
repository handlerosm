/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee/MonetDB OSM Handler
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *      Stefan de Konink <handlerosm@kinkrsoftware.nl>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <cherokee/common-internal.h>
#include "handler_osm.h"
#include "handler_osm_sql.h"
#include "handler_osm_db.h"
#include "handler_osm_get.h"
#include "handler_osm_put.h"
#include "handler_osm_delete.h"
#include "handler_osm_gpx.h"
#include <cherokee/cherokee.h>
#include <limits.h>

#include <axl.h>
#include <Mapi.h>

/* Plug-in initialization
 */
PLUGIN_INFO_HANDLER_EASIEST_INIT (osm, http_get | http_put | http_post | http_delete );


/* Methods implementation
 */
static ret_t 
props_free (cherokee_handler_osm_props_t *props)
{
	cherokee_buffer_mrproper(&props->db_hostname);
	cherokee_buffer_mrproper(&props->db_username);
	cherokee_buffer_mrproper(&props->db_password);
	cherokee_buffer_mrproper(&props->db_name);

//	CHEROKEE_RWLOCK_DESTROY (OSM_LOCK(props));
	
	if (!cherokee_list_empty(&(props->dbh_pool))) 
		cherokee_list_content_free(&(props->dbh_pool), (cherokee_list_free_func) mapi_destroy);

	return cherokee_module_props_free_base (MODULE_PROPS(props));
}

ret_t 
cherokee_handler_osm_configure (cherokee_config_node_t *conf, cherokee_server_t *srv, cherokee_module_props_t **_props)
{
	ret_t ret;
	Mapi db_connection;
	cherokee_list_t                      *i;
	cherokee_handler_osm_props_t *props;

	UNUSED(srv);

	if (*_props == NULL) {
		CHEROKEE_NEW_STRUCT (n, handler_osm_props);
//		CHEROKEE_NEW_STRUCT(m, handler_osm_priv);

		cherokee_module_props_init_base (MODULE_PROPS(n), 
						 MODULE_PROPS_FREE(props_free));
	
		n->read_only = true;
		n->db_port = 1025;
		cherokee_buffer_init (&n->db_hostname);
		cherokee_buffer_init (&n->db_username);
		cherokee_buffer_init (&n->db_password);
		cherokee_buffer_init (&n->db_name);

		INIT_LIST_HEAD(&(n->dbh_pool));
//		n->priv = m;
//		CHEROKEE_RWLOCK_INIT(OSM_LOCK(n), NULL);

		*_props = MODULE_PROPS(n);
	}

	props = PROP_OSM(*_props);

	cherokee_config_node_foreach (i, conf) {
		cherokee_config_node_t *subconf = CONFIG_NODE(i);

		if (equal_buf_str (&subconf->key, "db_hostname")) {
			cherokee_buffer_add_buffer (&props->db_hostname, &subconf->val);
		} else if (equal_buf_str (&subconf->key, "db_port")) {
			props->db_port = atoi(subconf->val.buf);
		} else if (equal_buf_str (&subconf->key, "db_username")) {
			cherokee_buffer_add_buffer (&props->db_username, &subconf->val);
		} else if (equal_buf_str (&subconf->key, "db_password")) {
			cherokee_buffer_add_buffer (&props->db_password, &subconf->val);
		} else if (equal_buf_str (&subconf->key, "db_name")) {
			cherokee_buffer_add_buffer (&props->db_name, &subconf->val);
		} else if (equal_buf_str (&subconf->key, "read_only")) {
			props->read_only = !!atoi (subconf->val.buf);
		} else {
			PRINT_MSG ("ERROR: Handler file: Unknown key: '%s'\n", subconf->key.buf);
			return ret_error;
		}
	}

	ret = handler_osm_db_connection_new(&props->db_hostname, 
			  		     props->db_port,
					    &props->db_username,
					    &props->db_password,
					    &props->db_name,
					    &db_connection);

	if (ret == ret_ok)
		ret = cherokee_list_add_content(&(props->dbh_pool), db_connection);

	return ret;
}

ret_t
cherokee_handler_osm_new  (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props)
{
	ret_t ret;
	CHEROKEE_NEW_STRUCT (n, handler_osm);
	
	/* Init the base class object
	 */
	cherokee_handler_init_base(HANDLER(n), cnt, HANDLER_PROPS(props), PLUGIN_INFO_HANDLER_PTR(osm));
	   
	MODULE(n)->init         = (handler_func_init_t) cherokee_handler_osm_init;
	MODULE(n)->free         = (module_func_free_t) cherokee_handler_osm_free;
	HANDLER(n)->step        = (handler_func_step_t) cherokee_handler_osm_step;
	HANDLER(n)->add_headers = (handler_func_add_headers_t) cherokee_handler_osm_add_headers;

	HANDLER(n)->support = hsupport_length;


	/* Init
	 */
	ret = cherokee_buffer_init (&n->buffer);
	if (unlikely(ret != ret_ok)) 
		return ret;

	ret = cherokee_buffer_ensure_size (&n->buffer, 4*1024);
	if (unlikely(ret != ret_ok)) 
		return ret;
	
	if (! axl_init ())
		return ret_error;
	
	*hdl = HANDLER(n);

	return ret_ok;
}

ret_t 
cherokee_handler_osm_init (cherokee_handler_osm_t *hdl)
{
	ret_t ret;

	cherokee_connection_t   *conn = HANDLER_CONN(hdl);

	TRACE("auth", "User: %s", (conn->realm_ref ? conn->realm_ref->buf : "none"));

	if (HDL_OSM_PROPS(hdl)->read_only && conn->header.method != http_get) {
		conn->error_code = http_access_denied;
		return ret_error;
	}

	ret = ret_error;	
	
#ifdef pooling
//	CHEROKEE_RWLOCK_WRITER (OSM_LOCK(HDL_OSM_PROPS(hdl)));
	if (!cherokee_list_empty(&(HDL_OSM_PROPS(hdl)->dbh_pool))) {
		TRACE ("list", "NOT EMPTY\n");
		if (cherokee_list_content_free_item_ptr(HDL_OSM_PROPS(hdl)->dbh_pool.next, (void **) &(hdl->dbh)) == ret_ok) {
			TRACE ("list", "FREE ELEMENT\n");
			if (mapi_ping(hdl->dbh) == 0 || mapi_reconnect(hdl->dbh) == 0) {
				ret = ret_ok;
			} else {
				mapi_destroy(hdl->dbh);
				TRACE("sql", "Unusable\n");
			}
		}
	} else {
		TRACE("list", "EMPTY\n");
	}
//	CHEROKEE_RWLOCK_UNLOCK (OSM_LOCK(HDL_OSM_PROPS(hdl)));

	if (ret == ret_error) {
		ret_t ret;
#endif
		ret = handler_osm_db_connection_new(&HDL_OSM_PROPS(hdl)->db_hostname, 
						     HDL_OSM_PROPS(hdl)->db_port,
						    &HDL_OSM_PROPS(hdl)->db_username,
						    &HDL_OSM_PROPS(hdl)->db_password,
						    &HDL_OSM_PROPS(hdl)->db_name,
						    (void *) &(hdl->dbh));
		if (unlikely(ret != ret_ok)) {
			hdl->dbh = NULL;
			conn->error_code = http_service_unavailable;
			TRACE("osm_db", "Making a new connection failed!");
			return ret_error;
		}
#ifdef pooling
	}
#endif

	switch (conn->header.method) {
		case http_get: {
			ret = cherokee_handler_osm_init_get(hdl);
			break;
		}
		case http_post:
		case http_put:
			ret = cherokee_handler_osm_init_put(hdl);
			break;
		case http_delete:
			ret = cherokee_handler_osm_init_delete(hdl);
			break;
		default:
			TRACE("osm", "%s did an other request\n", (conn->realm_ref ? conn->realm_ref->buf : "none"));
			conn->error_code = http_bad_request;
			ret = ret_error;
	}
	
	return ret;
}

ret_t 
cherokee_handler_osm_free (cherokee_handler_osm_t *hdl)
{
#ifdef pooling
	size_t free_connections;

	cherokee_list_get_len (&(HDL_OSM_PROPS(hdl)->dbh_pool), &free_connections);

	TRACE("list", "Current free connections: %d\n", free_connections);

	if (hdl->dbh) {
		if (free_connections > 10) /* make configurable */
#endif
			mapi_destroy(hdl->dbh);
#ifdef pooling
		else {
//			CHEROKEE_RWLOCK_WRITER (OSM_LOCK(HDL_OSM_PROPS(hdl)));
			cherokee_list_add_content(&(HDL_OSM_PROPS(hdl)->dbh_pool), (void *) hdl->dbh);
//			CHEROKEE_RWLOCK_UNLOCK (OSM_LOCK(HDL_OSM_PROPS(hdl)));
		}
	}

#endif

	cherokee_buffer_mrproper (&hdl->buffer);
	axl_end();
	return ret_ok;
}

ret_t 
cherokee_handler_osm_step (cherokee_handler_osm_t *hdl, cherokee_buffer_t *buffer)
{
 	cuint_t tosend;
	
	if (cherokee_buffer_is_empty (&hdl->buffer))
                return ret_eof;	

	tosend = (hdl->buffer.len > 1024 ? 1024 : hdl->buffer.len);

	cherokee_buffer_add (buffer, hdl->buffer.buf, tosend);
        cherokee_buffer_move_to_begin (&hdl->buffer, tosend);

	if (cherokee_buffer_is_empty (&hdl->buffer))
                return ret_eof_have_data;

        return ret_ok;
}


ret_t 
cherokee_handler_osm_add_headers (cherokee_handler_osm_t *hdl, cherokee_buffer_t *buffer)
{
	cherokee_buffer_add_str (buffer, "Content-Type: text/xml; charset=utf-8"CRLF);
//	cherokee_buffer_add_str (buffer, "Content-Disposition: attachment; filename=\"map.osm\""CRLF);
	cherokee_buffer_add_va (buffer, "Content-Length: %d"CRLF, hdl->buffer.len);
	return ret_ok;
}
