#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include "handler_osm.h"

typedef enum { OSM_DELETE_FIRST = 0, 
	       OSM_DELETE_NODE_ID,
	       OSM_DELETE_NODE_COMMAND,
	       OSM_DELETE_WAY_ID,
	       OSM_DELETE_WAY_COMMAND,
	       OSM_DELETE_RELATION_ID,
	       OSM_DELETE_RELATION_COMMAND,
	       OSM_DELETE_DONE } osm_state_delete_t;

ret_t cherokee_handler_osm_init_delete (cherokee_handler_osm_t *hdl);
ret_t delete_object_by_id(cherokee_handler_osm_t *hdl, unsigned long int id, osm_state_delete_t state);
