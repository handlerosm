#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include <Mapi.h>

#include "handler_osm.h"

ret_t handler_osm_db_connection_new(cherokee_buffer_t *host, cint_t port, cherokee_buffer_t *username, cherokee_buffer_t * password, cherokee_buffer_t *dbname, Mapi *connection);
ret_t cherokee_list_content_free_item_ptr (cherokee_list_t *head, void **item);

ret_t run_sql(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql, cherokee_buffer_t *buf, ret_t (*callback)());
ret_t run_sql2(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *buf, ret_t (*callback)());
ret_t run_sql3(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *sql3, cherokee_buffer_t *buf, ret_t (*callback)());
ret_t run_sql4(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *sql3, cherokee_buffer_t *sql4, cherokee_buffer_t *buf, ret_t (*callback)());
ret_t run_sql5(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *sql3, cherokee_buffer_t *sql4, cherokee_buffer_t *sql5, cherokee_buffer_t *buf, ret_t (*callback)());
