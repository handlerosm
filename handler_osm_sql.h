/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee/MonetDB OSM Handler
 *
 * SQL statements
 *
 * Authors:
 *      Stefan de Konink <handlerosm@kinkrsoftware.nl>
 */

#include <cherokee/common-internal.h>
#include "handler_osm.h"

#define CHANGES_TIME_DEFAULT 1
#define CHANGES_TIME_MAX     24

#define OSM_TAG         "osm"
#define OSM_VERSION     "0.5"
#define OSM_GENERATOR   "Cherokee/MonetDB OSM Server"
#define OSM_ATTRIBUTES  "version=\"" OSM_VERSION "\" generator=\"" OSM_GENERATOR "\""

#define NODE_TAG        "node"
#define NODE_ATTRIBUTES "id=\"%s\" lat=\"%s\" lon=\"%s\" visible = \"true\" user=\"%s\" timestamp=\"%s\""

#define TAG_TAG         "tag"
#define TAG_ATTRIBUTES  "k=\"%s\" v=\"%s\""

#define WAY_TAG         "way"
#define WAY_ATTRIBUTES  "id=\"%s\"  visible = \"true\" user=\"%s\" timestamp=\"%s\""

#define ND_TAG          "nd"
#define ND_ATTRIBUTES   "ref=\"%s\""

#define RELATION_TAG    "relation"
#define RELATION_ATTRIBUTES "id=\"%s\" visible = \"true\" user=\"%s\" timestamp=\"%s\""

#define MEMBER_TAG      "member"
#define MEMBER_ATTRIBUTES "type=\"%s\" ref=\"%s\" role=\"%s\""
#define MEMBER_TYPE_NODE "node"
#define MEMBER_TYPE_WAY "way"
#define MEMBER_TYPE_RELATION "relation"

#define XMLHEADER    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"CRLF
#define XML(THISTAG,LEVEL) LEVEL "<" THISTAG ## _TAG " " THISTAG ## _ATTRIBUTES
#define XMLCONTINUE   ">" CRLF
#define XMLCLOSESHORT "/>" CRLF
#define XMLCLOSE(THISTAG,LEVEL) LEVEL "</" THISTAG ## _TAG ">" CRLF

#define PREFERENCES_TAG   "preferences"
#define PREFERENCES_ATTRIBUTES ""
#define PREFERENCE_TAG    "preference"
#define PREFERENCE_ATTRIBUTES "k=\"%s\" v=\"%s\""


#define UINT_BITMAX "11930464.71111111"

#define SQL_NOW "", "now", ""

#ifdef LEGACY_SQL
	#define BBOX_VA_ARGS		top, bottom, left, right
	#define SQL_BY_BBOX	   	"lat BETWEEN %f AND %f AND long BETWEEN %f AND %f"
	#define SQL_NODES		"nodes_legacy"
	#define SQL_NODE_SELECT		"SELECT DISTINCT id, long, lat, usernames.username, timestamp "
#elif  LEGACY_SQL_UINT
	#define BBOX_VA_ARGS            top, bottom, left, right
	#define SQL_BY_BBOX             "lat BETWEEN (%f * " UINT_BITMAX ") AND (%f * " UINT_BITMAX ") AND long BETWEEN (%f * " UINT_BITMAX ") AND (%f * " UINT_BITMAX ")"
	#define SQL_NODES               "nodes_legacy_uint"
	#define SQL_NODE_SELECT         "SELECT DISTINCT id, CAST(long AS double) / " UINT_BITMAX ", CAST(lat AS double) / " UINT_BITMAX ", usernames.username, timestamp "
#else
	#define BBOX_VA_ARGS		bottom, left, bottom, right, top, right, top, left, bottom, left
	#define SQL_BY_BBOX	   	"WithIn(g, 'POLYGON((%f %f, %f %f, %f %f, %f %f, %f %f))') = TRUE"
	#define SQL_NODES		"nodes_legacy_gis"
	#define SQL_NODE_SELECT	   	"SELECT DISTINCT id, X(g), Y(g), usernames.username, timestamp"
#endif
//	#define SQL_NODE_CREATE_GET_ID	 "SELECT id FROM " SQL_NODES " ORDER BY id DESC LIMIT 1;"
//	#define SQL_NODE_CREATE		 "INSERT INTO " SQL_NODES " (long, lat, username, timestamp) VALUES (%f, %f, %d, %s)"
	#define SQL_NODE_CREATE		 "INSERT INTO " SQL_NODES " (long, lat, username, timestamp) VALUES (%f, %f, '%s', %s%s%s)"
	#define SQL_NODE_CREATE_BY_ID	 "INSERT INTO " SQL_NODES " (long, lat, username, timestamp, id) VALUES (%f, %f, '%s', %s%s%s, %d)"
	#define SQL_NODE_CREATE_NODE_TAG "INSERT INTO node_tags (node, k, v) VALUES (%d, '%s', '%s')"
	#define SQL_NODE_UPDATE_BY_ID	 "UPDATE " SQL_NODES " SET long = %f, lat = %f, username = '%s', timestamp = %s%s%s WHERE id = %d"
	#define SQL_NODE_UPDATE_NODE_TAG "UPDATE node_tags SET v = '%s' WHERE node = %d AND k = '%s'"

	#define SQL_NODE_DELETE_NODE_TAG "DELETE FROM node_tags WHERE node = %lu AND k NOT IN (%s)"

	#define SQL_RELATION_CREATE	 "INSERT INTO relations (username) VALUES ('%s')"
	#define SQL_RELATION_UPDATE      "UPDATE relations SET username = '%s', timestamp = %s%s%s WHERE id = %lu"
	#define SQL_RELATION_CREATE_NODE_TAG "INSERT INTO relation_tags (relation, k, v) VALUES (%d, '%s', '%s')"
	#define SQL_RELATION_UPDATE_NODE_TAG "UPDATE relation_tags SET v = '%s' WHERE relation = %d AND k = '%s'"
	#define SQL_RELATION_DELETE_NODE_TAG "DELETE FROM relation_tags WHERE relation = %d AND k NOT IN (%s)"

	#define SQL_WAY_CREATE		"INSERT INTO ways (username) VALUES ('%s')"
	#define SQL_WAY_UPDATE		"UPDATE ways SET username = '%s', timestamp = %s%s%s WHERE id = %lu"
	#define SQL_WAY_CREATE_NODE_TAG "INSERT INTO way_tags (walat, k, v) VALUES (%d, '%s', '%s')"
	#define SQL_WAY_UPDATE_NODE_TAG "UPDATE way_tags SET v = '%s' WHERE walat = %d AND k = '%s'"
	#define SQL_WAY_DELETE_NODE_TAG "DELETE FROM way_tags WHERE walat = %d AND k NOT IN (%s)"


	#define SQL_TRANSACTION_START	 "START TRANSACTION"
	#define SQL_TRANSACTION_COMMIT	 "COMMIT"
	#define SQL_TRANSACTION_ROLLBACK "ROLLBACK"


	#define SQL_NODE	   	SQL_NODE_SELECT \
				   	"FROM " SQL_NODES ", usernames "\
				   	"WHERE " SQL_NODES ".username = usernames.id"


	#define SQL_BY_ID		" id = %ld"
	#define SQL_ORDER_BY_ID		" ORDER BY id"

	#define SQL_NODE_BY_ID		SQL_NODE " AND " SQL_BY_ID
	#define SQL_NODE_BY_BBOX	SQL_NODE " AND " SQL_BY_BBOX

/*	#define SQL_NODE_BY_BBOX	SQL_NODE_SELECT \
						 "FROM " SQL_NODES ", members_node, usernames, members_node AS loc1, " SQL_NODES " AS loc2 "\
						 "WHERE " SQL_NODES ".id = members_node.to_node AND "\
						 	  SQL_NODES ".username = usernames.id AND "\
							"members_node.relation = loc1.relation AND "\
						 	"loc2.id = loc1.to_node AND loc2.lat > %f AND loc2.long > %f AND loc2.lat < %f AND loc2.long < %f" \
							"ORDER BY " SQL_NODES ".id"*/
	//#define SQL_NODE_BY_BBOX        "SELECT DISTINCT id, long, lat, '0', timestamp FROM nodes_legaclat, members_node, members_node AS loc1, nodes_legaclat AS loc2 WHERE nodes_legacy.id = members_node.to_node AND members_node.relation = loc1.relation AND loc2.id = loc1.to_node AND loc2.lat > %f AND loc2.long > %f AND loc2.lat < %f AND loc2.long < %f;"

// Oude
//	#define SQL_NODE_BY_BBOX	"SELECT DISTINCT id, long, lat, username, timestamp FROM nodes_legaclat, usernames, members_node WHERE nodes_legacy.username = usernames.id AND members_node.to_node = nodes_legacy.id AND members_node.relation IN (SELECT DISTINCT relation FROM members_node, nodes_legaclat WHERE members_node.to_node = nodes_legacy.id AND lat > %f AND long > %f AND lat < %f AND long < %f) ORDER BY nodes_legacy.id;"
	
	#define SQL_NODE_TAGS_SELECT	"SELECT node, k, v "
	#define SQL_NODE_TAGS		SQL_NODE_TAGS_SELECT " "\
					"FROM node_tags, " SQL_NODES " "\
					"WHERE node_tags.node = " SQL_NODES ".id"

	#define SQL_NODE_TAGS_BY_ID	SQL_NODE_TAGS " AND " SQL_BY_ID SQL_ORDER_BY_ID
	#define SQL_NODE_TAGS_BY_BBOX	SQL_NODE_TAGS " AND " SQL_BY_BBOX SQL_ORDER_BY_ID

	#define SQL_DELETE_NODE_BY_ID	"UPDATE " SQL_NODES " SET visible = FALSE WHERE id = %ld"

	#define SQL_RELATION_SELECT	"SELECT DISTINCT id, usernames.username, timestamp "
	#define SQL_RELATION 		SQL_RELATION_SELECT \
					"FROM relations, usernames "\
				   	"WHERE relations.username = usernames.id"
		
	#define SQL_RELATION_BY_NODE    SQL_RELATION_SELECT \
					"FROM relations, usernames, " SQL_NODES ", members_node "\
					"WHERE relations.username = usernames.id AND "\
					      SQL_NODES ".id = members_node.to_node AND members_node.relation = id"
	
	#define SQL_RELATION_BY_NODE_ID SQL_RELATION_BY_NODE " AND " SQL_NODES ".id = %ld" SQL_ORDER_BY_ID
	#define SQL_RELATION_BY_BBOX    SQL_RELATION_BY_NODE " AND " SQL_BY_BBOX SQL_ORDER_BY_ID
	#define SQL_RELATION_BY_ID	SQL_RELATION " AND id = %ld"


	#define SQL_REL_MEM_NOD_SELECT	"SELECT DISTINCT relation, to_node, role "
	#define SQL_RELATION_MEMBER_NODE SQL_REL_MEM_NOD_SELECT \
					 "FROM members_node "
	#define SQL_RELATION_MEMBER_NODE_BY_ID SQL_RELATION_MEMBER_NODE "WHERE relation=%ld"

	#define SQL_REL_MEM_REL_SELECT  "SELECT DISTINCT relation, to_relation, role "
	#define SQL_RELATION_MEMBER_RELATION SQL_REL_MEM_REL_SELECT \
					     "FROM members_relation "
					     
//	#define SQL_RELATION_MEMBER_RELATION_BY_ID SQL_RELATION_MEMBER_RELATION "WHERE relation=%ld"
	#define SQL_RELATION_MEMBER_RELATION_BY_ID "SELECT members_relation.relation, to_relation, role, (v='way') FROM members_relation LEFT JOIN relation_tags ON to_relation = relation_tags.relation WHERE k='type' AND v='way' AND members_relation.relation=%ld"

	#define SQL_RELATION_MEMBER_NODE_BY_BBOX SQL_REL_MEM_NOD_SELECT \
						 "FROM members_node, members_node AS constr, " SQL_NODES " "\
						 "WHERE members_node.relation = constr.relation AND "\
						 	SQL_NODES ".id = constr.to_node AND " SQL_BY_BBOX \
						      " ORDER BY relation, to_node"

	#define SQL_RELATION_MEMBER_RELATION_BY_BBOX SQL_REL_MEM_NOD_SELECT \
						     "FROM members_relation, members_node, " SQL_NODES " "\
						     "WHERE members_node.relation = members_relation.relation AND "\
						      SQL_NODES ".id = members_node.to_node AND " SQL_BY_BBOX " ORDER BY relation, to_node"

	#define SQL_RELATION_TAGS_SELECT "SELECT DISTINCT relation, k, v "
	#define SQL_RELATION_TAGS	SQL_RELATION_TAGS_SELECT " "\
					"FROM relation_tags "\
					"WHERE "

	#define SQL_ORDER_BY_RELATION   " ORDER BY relation"
	#define SQL_RELATION_TAGS_BY_ID	SQL_RELATION_TAGS "relation = %ld" SQL_ORDER_BY_RELATION

	#define SQL_RELATION_TAGS_BY_BBOX1 SQL_RELATION_TAGS_SELECT \
					"FROM " SQL_NODES ", members_node, relation_tags "\
					"WHERE "\
					       SQL_NODES ".id = members_node.to_node AND members_node.relation = relation_tags.relation AND "\
					       SQL_BY_BBOX
	#define SQL_RELATION_TAGS_BY_BBOX SQL_RELATION_TAGS_BY_BBOX1 SQL_ORDER_BY_RELATION


	#define SQL_DELETE_RELATION_BY_ID "UPDATE relations SET visible = FALSE WHERE id = %ld"
	#define SQL_DELETE_WAY_BY_ID "UPDATE ways SET visible = FALSE WHERE id = %ld"

	#define SQL_WAY			SQL_RELATION_SELECT \
				        "FROM relations, usernames, relation_tags AS constr "\
					"WHERE relations.username = usernames.id AND "\
					"constr.relation = relations.id AND constr.k='type' AND constr.v='way'"
	#define SQL_WAY_BY_ID		SQL_WAY " AND id=%ld"
	#define SQL_WAY_ND_SELECT	"SELECT DISTINCT relation, to_node "
	#define SQL_WAY_ND		SQL_WAY_ND_SELECT \
					"FROM members_node, relation_tags "\
					"WHERE relation_tags.relation = members_node.relation AND k='type' AND v='way'"

	#define SQL_WAY_ND_BY_ID	SQL_WAY_ND " AND relation=%ld ORDER BY relation, idx"
	#define SQL_WAY_BY_NODE		SQL_RELATION_SELECT \
					"FROM relations, usernames, " SQL_NODES ", members_node, relation_tags AS constr "\
					"WHERE relations.username = usernames.id AND constr.relation = id "\
					"AND constr.k='type' AND constr.v='way' AND "\
					SQL_NODES ".id = members_node.to_node AND members_node.relation = id"
	#define SQL_WAY_BY_NODE_ID	SQL_WAY_BY_NODE " AND " SQL_NODES ".id = %ld ORDER BY id"
	#define SQL_WAY_BY_BBOX		SQL_WAY_BY_NODE "AND " SQL_BY_BBOX " ORDER BY id"
/*	#define SQL_ND_BY_BBOX		SQL_WAY_ND_SELECT \
					"FROM members_node, relation_tags, " SQL_NODES " "\
					"WHERE relation_tags.relation = members_node.relation AND k='type' AND v='way' "\
					"AND " SQL_NODES ".id = members_node.to_node" SQL_BY_BBOX " ORDER BY relation, idx"*/
//	#define SQL_ND_BY_BBOX		"SELECT relation, to_node FROM members_node WHERE relation IN (SELECT DISTINCT relation FROM relation_tags, members_node, nodes_legaclat WHERE relation_tags.relation = members_node.relation AND members_node.to_node = nodes_legacy.id AND lat > %f AND long > %f AND lat < %f AND long < %f AND relation_tags.k = 'type' AND relation_tags.v = 'way') ORDER BY members_node.relation, members_node.idx;"
//

	#define SQL_ND_BY_BBOX		"SELECT relation, to_node FROM members_node WHERE relation IN (SELECT DISTINCT relations.id FROM nodes_legaclat, members_node, relations, relation_tags WHERE relations.id = relation_tags.relation AND members_node.relation = relations.id AND members_node.to_node = nodes_legacy.id AND long > %f AND lat > %f AND long < %f AND lat < %f AND k = 'type' AND v = 'way') ORDER BY relation, idx"

//	#define SQL_ND_BY_BBOX		"SELECT relation, to_node FROM members_node WHERE relation IN (SELECT id FROM relations WHERE id IN (SELECT DISTINCT relation FROM relation_tags, members_node, nodes_legaclat WHERE relation_tags.relation = members_node.relation AND members_node.to_node = nodes_legacy.id AND lat > %f AND long > %f AND lat < %f AND long < %f AND relation_tags.k = 'type' AND relation_tags.v = 'way')) ORDER BY members_node.relation, members_node.idx;"

	#define SQL_WAY_TAGS_BY_ID	SQL_RELATION_TAGS_BY_ID
	#define SQL_WAY_TAGS_BY_BBOX	SQL_RELATION_TAGS_BY_BBOX1 SQL_ORDER_BY_RELATION
//	#define SQL_WAY_TAGS_BY_BBOX	SQL_RELATION_TAGS_BY_BBOX1 " AND k<>'type' AND v<>'way' " SQL_ORDER_BY_RELATION

	#define SQL_USERID_BY_NAME	"SELECT '0';"

	#define SQL_NODE_EXIST		"SELECT id FROM " SQL_NODES " WHERE id = %d;"
	#define SQL_WAY_EXIST		"SELECT id FROM ways WHERE id = %d;"
	#define SQL_RELATION_EXIST	"SELECT id FROM relations WHERE id = %d;"
