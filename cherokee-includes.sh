#!/bin/sh

if [ $# -eq 2 ]; then
	cd $1
	cp config.h constants.h $2/include/cherokee/.
	cd cherokee
	cat common-internal.h | sed "s/\(<con\(.*\)\.h>\)/\"con\2.h\"/g" > $2/include/cherokee/common-internal.h
	cp bind.h threading.h cryptor.h socket.h iocache.h header-protected.h virtual_server.h virtual_server_names.h rule_list.h regex.h connection-protected.h $2/include/cherokee/.

	mkdir -p $2/include/cherokee/pcre
	cp pcre/*.h $2/include/cherokee/pcre/.

	echo -e "#include <sys/uio.h>\n#include <stdint.h>\n" > $2/include/cherokee/util.h
	cat util.h >> $2/include/cherokee/util.h
	echo "#include <cherokee/connection-protected.h>" >> $2/include/cherokee/cherokee.h

else
	echo $0 "<cherokee-sourcedir> <cherokee-install-prefix>"
fi
