#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include "handler_osm_user.h"
#include "handler_osm_sql.h"

void user_preferences(cherokee_buffer_t *buf)
{
	cherokee_buffer_t content = CHEROKEE_BUF_INIT;
	cherokee_buffer_add_str (buf, XML(PREFERENCES, "  "));
	if (content.len > 0) {
		cherokee_buffer_add_str (buf, XMLCONTINUE);
		cherokee_buffer_add_buffer (buf, &content);
		cherokee_buffer_mrproper (&content);
		cherokee_buffer_add_str (buf, XMLCLOSE(PREFERENCES, "  "));
	} else 
		cherokee_buffer_add_str (buf, XMLCLOSESHORT);

	printf("%s\n", __func__);
}


