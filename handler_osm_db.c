#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include "handler_osm_db.h"
#include <Mapi.h>

ret_t handler_osm_db_connection_new(cherokee_buffer_t *host, cint_t port, cherokee_buffer_t *username, cherokee_buffer_t * password, cherokee_buffer_t *dbname, Mapi *connection) {
	*connection = mapi_connect(host->buf, port, username->buf, password->buf, "sql", dbname->buf);
	
	if (!mapi_error(*connection)) {
		return ret_ok;
	}

	mapi_explain(*connection, stderr);
	
	return ret_error;
}

ret_t
cherokee_list_content_free_item_ptr (cherokee_list_t *head, void **item)
{
	cherokee_list_del (head);
	*item = LIST_ITEM(head)->info;

        free (head);
        return ret_ok;
}

ret_t
run_sql(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql, cherokee_buffer_t *buf, ret_t (*callback)()) {
	ret_t ret = ret_error;
	MapiHdl mapi_hdl = NULL;
	TRACE("sql", "%s\n", sql->buf);
	if ((mapi_hdl = mapi_query(hdl->dbh, sql->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
		TRACE("sql", "DONE\n");
		if (callback != NULL)
			ret = callback(hdl, &mapi_hdl, buf);
		else
			ret = ret_ok;
		mapi_close_handle(mapi_hdl);
	} else
		mapi_explain(hdl->dbh, stderr);
	
	return ret;
}


ret_t
run_sql2(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *buf, ret_t (*callback)()) {
	ret_t ret = ret_error;
	MapiHdl mapi_hdl1 = NULL;
	MapiHdl mapi_hdl2 = NULL;

	TRACE("sql", "%s\n", sql1->buf);
	if ((mapi_hdl1 = mapi_query(hdl->dbh, sql1->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
		TRACE("sql", "%s\n", sql2->buf);
		if ((mapi_hdl2 = mapi_query(hdl->dbh, sql2->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
			TRACE("sql", "DONE\n");
			if (callback != NULL)
				ret = callback(hdl, &mapi_hdl1, &mapi_hdl2, buf);
			else 
				ret = ret_ok;
			mapi_close_handle(mapi_hdl2);
		} else
			mapi_explain(hdl->dbh, stderr);
		mapi_close_handle(mapi_hdl1);
	} else
		mapi_explain(hdl->dbh, stderr);
	
	return ret;
}

ret_t
run_sql3(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *sql3, cherokee_buffer_t *buf, ret_t (*callback)()) {
	ret_t ret = ret_error;
	MapiHdl mapi_hdl1 = NULL;
	MapiHdl mapi_hdl2 = NULL;
	MapiHdl mapi_hdl3 = NULL;

	TRACE("sql", "%s\n", sql1->buf);
	if ((mapi_hdl1 = mapi_query(hdl->dbh, sql1->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
		TRACE("sql", "%s\n", sql2->buf);
		if ((mapi_hdl2 = mapi_query(hdl->dbh, sql2->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
			TRACE("sql", "%s\n", sql3->buf);
			if ((mapi_hdl3 = mapi_query(hdl->dbh, sql3->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
				TRACE("sql", "DONE\n");
				if (callback != NULL)
					ret = callback(hdl, &mapi_hdl1, &mapi_hdl2, &mapi_hdl3, buf);
				else
					ret = ret_ok;
//				mapi_cache_freeup(mapi_hdl3, 100);
				mapi_close_handle(mapi_hdl3);
			} else
				mapi_explain(hdl->dbh, stderr);
			mapi_close_handle(mapi_hdl2);
		} else
			mapi_explain(hdl->dbh, stderr);
		mapi_close_handle(mapi_hdl1);
	} else
		mapi_explain(hdl->dbh, stderr);


	return ret;
}

ret_t
run_sql4(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *sql3, cherokee_buffer_t *sql4, cherokee_buffer_t *buf, ret_t (*callback)()) {
	ret_t ret = ret_error;
	MapiHdl mapi_hdl1 = NULL;
	MapiHdl mapi_hdl2 = NULL;
	MapiHdl mapi_hdl3 = NULL;
	MapiHdl mapi_hdl4 = NULL;

	TRACE("sql", "%s\n", sql1->buf);
	if ((mapi_hdl1 = mapi_query(hdl->dbh, sql1->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
		TRACE("sql", "%s\n", sql2->buf);
		if ((mapi_hdl2 = mapi_query(hdl->dbh, sql2->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
			TRACE("sql", "%s\n", sql3->buf);
			if ((mapi_hdl3 = mapi_query(hdl->dbh, sql3->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
				TRACE("sql", "%s\n", sql4->buf);
				if ((mapi_hdl4 = mapi_query(hdl->dbh, sql4->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
					TRACE("sql", "DONE\n");
					if (callback != NULL)
						ret = callback(hdl, &mapi_hdl1, &mapi_hdl2, &mapi_hdl3, &mapi_hdl4, buf);
					else 
						ret = ret_ok;
//					mapi_cache_freeup(mapi_hdl4, 100);
					mapi_close_handle(mapi_hdl4);
				} else
					mapi_explain(hdl->dbh, stderr);
				mapi_close_handle(mapi_hdl3);
			} else
				mapi_explain(hdl->dbh, stderr);
			mapi_close_handle(mapi_hdl2);
		} else
			mapi_explain(hdl->dbh, stderr);
		mapi_close_handle(mapi_hdl1);
	} else
		mapi_explain(hdl->dbh, stderr);

	return ret;
}

ret_t
run_sql5(cherokee_handler_osm_t *hdl, cherokee_buffer_t *sql1, cherokee_buffer_t *sql2, cherokee_buffer_t *sql3, cherokee_buffer_t *sql4, cherokee_buffer_t *sql5, cherokee_buffer_t *buf, ret_t (*callback)()) {
	ret_t ret = ret_error;
	MapiHdl mapi_hdl1 = NULL;
	MapiHdl mapi_hdl2 = NULL;
	MapiHdl mapi_hdl3 = NULL;
	MapiHdl mapi_hdl4 = NULL;
	MapiHdl mapi_hdl5 = NULL;

	TRACE("sql", "%s\n", sql1->buf);
	if ((mapi_hdl1 = mapi_query(hdl->dbh, sql1->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
		TRACE("sql", "%s\n", sql2->buf);
		if ((mapi_hdl2 = mapi_query(hdl->dbh, sql2->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
			TRACE("sql", "%s\n", sql3->buf);
			if ((mapi_hdl3 = mapi_query(hdl->dbh, sql3->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
				TRACE("sql", "%s\n", sql4->buf);
				if ((mapi_hdl4 = mapi_query(hdl->dbh, sql4->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
					TRACE("sql", "%s\n", sql5->buf);
					if ((mapi_hdl5 = mapi_query(hdl->dbh, sql5->buf)) != NULL && mapi_error(hdl->dbh) == MOK) {
						TRACE("sql", "DONE\n");
						if (callback != NULL)
							ret = callback(hdl, &mapi_hdl1, &mapi_hdl2, &mapi_hdl3, &mapi_hdl4, &mapi_hdl5, buf);
						else 
							ret = ret_ok;
//						mapi_cache_freeup(mapi_hdl5, 100);
						mapi_close_handle(mapi_hdl5);
					} else
						mapi_explain(hdl->dbh, stderr);
					mapi_close_handle(mapi_hdl4);
				} else
					mapi_explain(hdl->dbh, stderr);
				mapi_close_handle(mapi_hdl3);
			} else
				mapi_explain(hdl->dbh, stderr);
			mapi_close_handle(mapi_hdl2);
		} else
			mapi_explain(hdl->dbh, stderr);
		mapi_close_handle(mapi_hdl1);
	} else
		mapi_explain(hdl->dbh, stderr);

	return ret;
}

