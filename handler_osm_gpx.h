#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>

void gpx_data(cherokee_buffer_t *buf, unsigned long int id);
void gpx_details(cherokee_buffer_t *buf, unsigned long int id);

