#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "osmbinary.h"

void writenode(int bi, node_t *this) {
	write(bi, "N", 1);
	write(bi, &this->id, sizeof(this->id));
	write(bi, &this->lat, sizeof(this->lat));
	write(bi, &this->lon, sizeof(this->lon));
	write(bi, &this->stamp, sizeof(this->stamp));
	write(bi, &this->userlen, sizeof(this->userlen));
	write(bi, this->user, this->userlen);
}

void writetag(int bi, tag_t *this) {
	write(bi, "T", 1);
	write(bi, &this->klen, sizeof(this->klen));
	write(bi, this->k, this->klen);
	write(bi, &this->vlen, sizeof(this->vlen));
	write(bi, this->v, this->vlen);
}

void writeother(int bi, other_t *this, char type) {
	write(bi, &type, 1);
	write(bi, &this->id, sizeof(this->id));
	write(bi, &this->stamp, sizeof(this->stamp));
	write(bi, &this->userlen, sizeof(this->userlen));
	write(bi, this->user, this->userlen);	
}

void writend(int bi, nd_t *this) {
	write(bi, "D", 1);
	write(bi, &this->id, sizeof(this->id));
}

void writemember(int bi, member_t *this) {
	write(bi, "M", 1);
	write(bi, &this->typelen, sizeof(this->typelen));
	write(bi, this->type, this->typelen);
	write(bi, &this->reflen, sizeof(this->reflen));
	write(bi, this->ref, this->reflen);
	write(bi, &this->rolelen, sizeof(this->rolelen));
	write(bi, this->role, this->rolelen);
}
