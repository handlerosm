#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>

#include "osmbinary.h"

/*
 * <osm>
 *  <node>
 *   <tag k=".." v=".." />
 *  </node>
 *  <way>
 *   <nd>
 *   <tag>
 *  </way>
 *  <relation>
 *   <member>
 *   <tag>
 *  </relation>
 */

static void parser(char *range, unsigned long int max) {
    typedef enum { OSM = 0, NODE = 1, WAY = 2, RELATION = 3, TAG = 4, ND = 5, MEMBER = 6 } osm_state_t;
    typedef enum { UNKNOWN = 0, ID, LAT, LON, USER, TIMESTAMP, KEY, VALUE, TYPE, REF, ROLE} key_state_t;
    char *attr_id = NULL, *attr_lat = NULL, *attr_lon = NULL, *attr_user = NULL,  *attr_timestamp = NULL, *attr_key = NULL, *attr_value = NULL,
         *attr_type = NULL, *attr_ref = NULL, *attr_role = NULL;

//    unsigned long int count_nodes = 0, count_node_tags = 0,
//    count_ways = 0, count_way_tags = 0, count_way_nds = 0,
//    count_relations = 0, count_relation_tags = 0, count_members_node = 0, count_members_relation = 0, count_members_way = 0;

//    unsigned long int sequence = 0;

    int bi;
    node_t   bin_node;
    other_t  bin_other;
    tag_t    bin_tag;
    nd_t     bin_nd;
    member_t bin_member;
    struct tm bin_tmp;

    unlink("osm.bin");
    bi = open("osm.bin", O_WRONLY | O_CREAT, S_IRUSR| S_IWUSR|S_IRGRP|S_IROTH);

    osm_state_t current_tag = OSM;
    osm_state_t parent_tag = OSM;

    char *start, *end, *nodename, *nodename_end;

    start = range;
    end = strchrnul((const char*) start, '\n');

    if (strncmp(start, "<?xml", 5) != 0)
        return;

    start = end + 1;
    end = strchrnul((const char*) start, '\n');

    if (strncmp(start, "<osm", 4) != 0)
        return;

    start = end + 1;

    do {
    end = strchrnul((const char*) start, '\n');

    nodename = strchrnul(start, '<') + 1;
    nodename_end = strchrnul(nodename, ' ');

    if (nodename[0] == '/') {
        free(attr_id);
        free(attr_lat);
        free(attr_lon);
        free(attr_timestamp);
        free(attr_user);
        
        attr_id = NULL;
        attr_lat = NULL;
        attr_lon = NULL;
        attr_user = NULL;
        attr_timestamp = NULL;

//	sequence = 0;

        start = end + 1;
        continue;
    }

    switch (nodename_end - nodename) {
        case 2:
            current_tag = ND;
            break;
        case 3: {
                    switch (nodename[0]) {
                        case 'o':
                            current_tag = OSM;  
                            break;
                        case 'w':
                            current_tag = WAY;
                            break;
                        case 't':
                            current_tag = TAG;
                            break;
                        default:
                            fprintf(stderr, "--> %c%c", nodename[0], nodename[1]);
                    }
                    break;
                }
        case 4:
                current_tag = NODE;
                break;
        case 6:
                current_tag = MEMBER;
                break;
        case 8:
                current_tag = RELATION;
                break;
        default:
                fprintf(stderr, "--> %c%c", nodename[0], nodename[1]);
    }


    char *key, *key_end, *value_end;
    key = nodename_end + 1;

    do {
        char *value;
        key_state_t current_key = UNKNOWN;
        key_end = strchrnul(key, '=');

        if (key_end == NULL || key_end >= end)
		break;

        switch (key_end - key) {
            case 1: {
                    switch (key[0]) {
                        case 'k':
                            current_key = KEY;
                            break;
                        case 'v':
                            current_key = VALUE;
                            break;
                        default:
                            current_key = UNKNOWN;
                    }
                    break;
            }   
            case 2:
                current_key = ID;
                break;
            case 3: {
                        switch (key[1]) {
                            case 'a':
                                current_key = LAT;
                                break;
                            case 'o':
                                current_key = LON;
                                break;
			    case 'e':
			    	current_key = REF;
				break;
                            default:
                                current_key = UNKNOWN;
                                fprintf(stderr, "--> %c%c\n", key[0], key[1]);
                        }
                        break;
                    }
            case 4: {
	    		switch (key[0]) {
			    case 'u':
			    	current_key = USER;
				break;
			    case 'r':
			    	current_key = ROLE;
				break;
			    case 't':
			    	current_key = TYPE;
				break;
			    default:
			    	current_key = UNKNOWN;
				fprintf(stderr, "--> %c%c\n", key[0], key[1]);
			}
			break;
		}
            case 9:
                    current_key = TIMESTAMP;
                    break;
            default: {
                    char *thingie = strndup(key, (key_end - key));
                    current_key = UNKNOWN;
                    
                    fprintf(stderr, "UNKNOWN ATTR %s-> %c%c\n", thingie, key[0], key[1]);
                    free(thingie);
                }
        }

        value = key_end + 2;
	value_end = value;
	value_end = strchr(value_end, '"');

	if (value_end > end)
		break;

        switch (current_key) {
            case ID:
                if (attr_id) free(attr_id);
                attr_id = strndup(value, (value_end - value));
                break;

            case LAT:
                if (attr_lat) free(attr_lat);
                attr_lat = strndup(value, (value_end - value));
                break;

            case LON:
                if (attr_lon) free(attr_lon);
                attr_lon = strndup(value, (value_end - value));
                break;

            case TIMESTAMP:
                if (attr_timestamp) free(attr_timestamp);
                attr_timestamp = strndup(value, (value_end - value));
//		attr_timestamp[10] = ' '; /* Stupid timestamp fix */
                break;

            case USER: {
	    	char *tmp;
                if (attr_user) free(attr_user);
                attr_user = strndup(value, (value_end - value));
//		tmp = escape_string(attr_user);
//		free(attr_user);
//		attr_user = tmp;
                break;
	    }

            case KEY: {
	    	char *tmp;
                if (attr_key) free(attr_key);
                attr_key = strndup(value, (value_end - value));
//		tmp = escape_string(attr_key);
//		free(attr_key);
//		attr_key = tmp;
                break;
	    }
            
            case VALUE: {
	    	char *tmp;
                if (attr_value) free(attr_value);
                attr_value = strndup(value, (value_end - value));
//		tmp = escape_string(attr_value);
//		free(attr_value);
//		attr_value = tmp;
                break;
	    }

            case TYPE:
                if (attr_type) free(attr_type);
                attr_type = strndup(value, (value_end - value));
                break;

            case REF:
                if (attr_ref) free(attr_ref);
                attr_ref = strndup(value, (value_end - value));
                break;

            case ROLE: {
	    	char *tmp;
                if (attr_role) free(attr_role);
		attr_role = strndup(value, (value_end - value));
//		tmp = escape_string(attr_role);
//		free(attr_role);
//		attr_role = tmp;
                break;
	    }

            default:
                fprintf(stderr, "--> %c%c\n", value[0], value[1]);
        }

        key = value_end + 2;
    } while (key < end);

    switch (current_tag) {
        case NODE:
	    bin_node.id = strtoul(attr_id, (char **) NULL, 10);
	    bin_node.lon = strtof(attr_lon, (char **) NULL);
	    bin_node.lat = strtof(attr_lat, (char **) NULL);
	    if (!attr_user) {
	    	bin_node.userlen = 0;
		bin_node.user = NULL;
	    } else {
	        bin_node.userlen = strlen(attr_user);
  	        bin_node.user = attr_user;
	    }
	    strptime(attr_timestamp, "%FT%T%Z", &bin_tmp);
	    bin_node.stamp = mktime(&bin_tmp);
	    writenode(bi, &bin_node);
//          fprintf(fd_nodes, "%s, %s, %s, '%s', %s\n", attr_id, attr_lat, attr_lon, attr_user, attr_timestamp);
//	    count_nodes++;
            break;
        case TAG: {
	    bin_tag.klen = strlen(attr_key);
	    bin_tag.k = attr_key;
	    bin_tag.vlen = strlen(attr_value);
	    bin_tag.v = attr_value;
	    writetag(bi, &bin_tag);

/*		switch (parent_tag) {
			case NODE:
            		    fprintf(fd_node_tags, "%s, '%s', '%s'\n", attr_id, attr_key, attr_value);
		  	    count_node_tags++;
		            break;
			case WAY:
			    fprintf(fd_way_tags, "%s, '%s', '%s'\n", attr_id, attr_key, attr_value);
			    count_way_tags++;
			    break;
			case RELATION:
			    fprintf(fd_relation_tags, "%s, '%s', '%s'\n", attr_id, attr_key, attr_value);
			    count_relation_tags++;
			    break;
			default:
		 	    break;
		}*/
		break;
	}
        case WAY:
	    bin_other.id = strtoul(attr_id, (char **) NULL, 10);
	    if (!attr_user) {
	    	bin_other.userlen = 0;
		bin_other.user = NULL;
	    } else {
	        bin_other.userlen = strlen(attr_user);
  	        bin_other.user = attr_user;
	    }
	    strptime(attr_timestamp, "%FT%T%Z", &bin_tmp);
	    bin_other.stamp = mktime(&bin_tmp);
	    writeother(bi, &bin_other, 'W');

//          fprintf(fd_ways, "%s, '%s', '%s'\n", attr_id, attr_user, attr_timestamp);
//	    count_ways++;
//          fprintf(fd_way_tags, "%s, '%s', '%s'\n", attr_id, "type", "way");
//	    count_way_tags++;
            break;
        case RELATION:
	    bin_other.id = strtoul(attr_id, (char **) NULL, 10);
	    if (!attr_user) {
	    	bin_other.userlen = 0;
		bin_other.user = NULL;
	    } else {
	        bin_other.userlen = strlen(attr_user);
  	        bin_other.user = attr_user;
	    }
	    strptime(attr_timestamp, "%FT%T%Z", &bin_tmp);
	    bin_other.stamp = mktime(&bin_tmp);
	    writeother(bi, &bin_other, 'R');

//            fprintf(fd_relations, "%s, '%s', '%s'\n", attr_id, attr_user, attr_timestamp);
//	    count_relations++;
            break;
        case MEMBER:
	    bin_member.type = attr_type;
	    bin_member.typelen = strlen(attr_type);
	    bin_member.ref = attr_ref;
	    bin_member.reflen = strlen(attr_ref);
	    bin_member.role = attr_role;
	    bin_member.rolelen = strlen(attr_role);
	    writemember(bi, &bin_member);

/*                if (strcmp(attr_type, "node") == 0) {
                        fprintf(fd_members_node, "%s, %lu, %s, '%s'\n", attr_id, sequence, attr_ref, attr_role);
			count_members_node++;
		} else if (strcmp(attr_type, "way") == 0) {
                        fprintf(fd_members_way, "%s, %lu, %s, '%s'\n", attr_id, sequence, attr_ref, attr_role);
			count_members_way++;
		} else if (strcmp(attr_type, "relation") == 0) {
                        fprintf(fd_members_relation, "%s, %lu, %s, '%s'\n", attr_id, sequence, attr_ref, attr_role);
			count_members_relation++;
		}
		sequence++;*/
                break;
        case ND:
	    bin_nd.id = strtoul(attr_id, (char **) NULL, 10);
	    writend(bi, &bin_nd);
/*            fprintf(fd_way_nds, "%s, %lu, %s\n", attr_id, sequence, attr_ref);
	    sequence++;
	    count_way_nds++;
            break;*/
        default:
            break;
    }
    
    if (end[-2] == '/') {
        switch (current_tag) {
            case NODE:
                free(attr_lat);
                free(attr_lon);
                attr_lat = NULL;
                attr_lon = NULL;
		/* no break! */

	    case WAY:
	    case RELATION:
                free(attr_id);
                free(attr_timestamp);
                free(attr_user);

                attr_id = NULL;
                attr_user = NULL;
                attr_timestamp = NULL;

//	    	sequence = 0;
                break;

            case TAG:
                free(attr_key);
                free(attr_value);

                attr_key = NULL;
                attr_value = NULL;
                break;
	    
	    case ND:
	    case MEMBER:
	    	free(attr_type);
	    	free(attr_ref);
	    	free(attr_role);

		attr_type = NULL;
		attr_ref = NULL;
		attr_role = NULL;
	    default:
	    	break;
        }
    } else if (current_tag == NODE || current_tag == WAY || current_tag == RELATION) {
		parent_tag = current_tag;
	}

    } while ((start = ++end) < (range + max));
        
    free(attr_id);
    free(attr_lat);
    free(attr_lon);
    free(attr_timestamp);
    free(attr_user);

    free(attr_key);
    free(attr_value);

    close(bi);
}


int main(int argc, char *argv[]) {
    int fd;
    struct stat statbuf;

    if (argc != 2)
        exit(-1);

    fprintf(stderr, "Analysing %s...\n", argv[1]);

    fd = open(argv[1], O_RDONLY);

    if (fd < 0)
        exit(-1);

    if (fstat (fd, &statbuf) == -1) { perror("fstat:"); exit(-1); }

    if (statbuf.st_size > 0) {
        char *range = NULL;
        range = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED, fd, (off_t) 0);
	if (range == MAP_FAILED) { perror("Mmap:"); printf("(did you compile PAE in the kernel?)\n"); exit(-1); }
        parser(range, statbuf.st_size / sizeof(char));
        munmap(range, statbuf.st_size);
    }

    close(fd);
    exit(0);
}
