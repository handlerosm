#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>

#include "osmbinary.h"

int main(int argc, char *argv[]) {
    int fd;
    struct stat statbuf;

    if (argc != 2)
        exit(-1);

    fprintf(stderr, "Analysing %s...\n", argv[1]);

    fd = open(argv[1], O_RDONLY);

    if (fd < 0)
        exit(-1);

    if (fstat (fd, &statbuf) == -1) { perror("fstat:"); exit(-1); }

    if (statbuf.st_size > 0) {
        char *range = NULL;
        range = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED, fd, (off_t) 0);
        if (range == MAP_FAILED) { perror("Mmap:"); printf("(did you compile PAE in the kernel?)\n"); exit(-1); }

	int i = 0;

	while (i < statbuf.st_size) {
		int j;
		node_t bin_node;
		tag_t bin_tag;
		other_t bin_other;
		nd_t bin_nd;
		member_t bin_member;

		switch (range[i++]) {
			case 'N': {
				unsigned char *len;
//				fprintf(stderr, "\nindex: %d\n", i);
				i += sizeof(bin_node.id) +
				     sizeof(bin_node.lat) +
				     sizeof(bin_node.lon) +
				     sizeof(bin_node.stamp);

                                len = (unsigned char *) &range[i];
				i += sizeof(bin_node.userlen);
				
				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				
				putchar('\n');

				i += *len;
				break;
			}
			case 'T': {
				unsigned short int *len;

				len = (unsigned short int *) &range[i];
				i += sizeof(bin_tag.klen);

				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				i += *len;
				putchar('\n');

				len = (unsigned short int *) &range[i];
				i += sizeof(bin_tag.vlen);

				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				i += *len;
				putchar('\n');
				
				break;
			}
			case 'W': {
				unsigned char *len;
				i += sizeof(bin_other.id) +
				     sizeof(bin_other.stamp);

				len = (unsigned char *) &range[i];
				i += sizeof(bin_other.userlen);
				
				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				i += *len;
				putchar('\n');
				break;
			}
			case 'D': {
				i += sizeof(bin_nd.id);	
				break;
			}
			case 'R': {
				unsigned char *len;
				i += sizeof(bin_other.id) +
				     sizeof(bin_other.stamp);

				len = (unsigned char *) &range[i];
				i += sizeof(bin_other.userlen);
				
				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				i += *len;
				putchar('\n');
				break;
			}
			case 'M': {
				unsigned short int *len;

				len = (unsigned short int *) &range[i];
				i += sizeof(bin_member.typelen);
				
				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				i += *len;
				putchar('\n');
				
				len = (unsigned short int *) &range[i];
				i += sizeof(bin_member.reflen);
				
				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				i += *len;
				putchar('\n');
				
				len = (unsigned short int *) &range[i];
				i += sizeof(bin_member.rolelen);
				
				for (j = 0; j < *len; j++)
					putchar(range[i + j]);
				i += *len;
				putchar('\n');
				break;
			}
			default:
				fprintf(stderr, "otherstate");
				i = statbuf.st_size;
		}
	}

        munmap(range, statbuf.st_size);
    }

    close(fd);
    exit(0);
}
