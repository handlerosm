#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
        unsigned long id;
        float lat, lon;
        time_t stamp;
        unsigned char userlen;
        char *user;
} node_t;

typedef struct {
        unsigned short int klen;
        char *k;
        unsigned short int vlen;
        char *v;
} tag_t;

typedef struct {
        unsigned long id;
} nd_t;

typedef struct {
        unsigned long id;
        time_t stamp;
        unsigned char userlen;
        char *user;
} other_t;

typedef struct {
        unsigned short int typelen;
        char *type;
        unsigned short int reflen;
        char *ref;
        unsigned short int rolelen;
        char *role;
} member_t;

void writenode(int bi, node_t *this);
void writetag(int bi, tag_t *this);
void writeother(int bi, other_t *this, char type);
void writend(int bi, nd_t *this);
void writemember(int bi, member_t *this);
