all:
	gcc -O2 -I/opt/monetdb/include/MonetDB/mapilib -L/opt/monetdb/lib -D_GNU_SOURCE -Wall -lMapi -Wl,-rpath=/opt/monetdb/lib -o osmparser osmparser.c 
	gcc -Wall -Dpooling -DHAVE_STDINT_H -DHAVE_SYS_UIO_H -DHAVE_PWD_H -DHAVE_GRP_H -DTRACE_ENABLED -D_GNU_SOURCE -g -fPIC -shared -I/opt/monetdb/include/MonetDB/mapilib -I/opt/cherokee/include -I/opt/cherokee/include/cherokee -I/opt/cherokee/include/axl -L/opt/monetdb/lib -L/opt/cherokee/lib -lMapi -laxl -Wl,-rpath=/opt/cherokee/lib -Wl,-rpath=/opt/monetdb/lib handler_osm.c handler_osm_db.c handler_osm_get.c handler_osm_put.c handler_osm_delete.c handler_osm_gpx.c handler_osm_user.c -o libplugin_osm.so

install:
	cp osmparser /opt/cherokee/bin/.
	cp *.so /opt/cherokee/lib/cherokee/.
	cp Module*.py /opt/cherokee/share/cherokee/admin/.
