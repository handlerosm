#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include <limits.h>


#include "handler_osm.h"
#include "handler_osm_get.h"
#include "handler_osm_sql.h"
#include "handler_osm_db.h"
#include "handler_osm_gpx.h"
#include "handler_osm_user.h"

#define SQL_XAPI_NODE     "SELECT DISTINCT id, long, lat, username, timestamp FROM " SQL_NODES ", node_tags WHERE id = node"
#define SQL_XAPI_NODE_END " ORDER BY id"

#define SQL_XAPI_NODE_ID  "SELECT DISTINCT id FROM " SQL_NODES ", node_tags WHERE id = node"

#define SQL_XAPI_NTAG     "SELECT node, k, v FROM node_tags WHERE node IN ("
#define SQL_XAPI_NTAG_END ") ORDER BY node" 

#define SQL_XAPI_WAY	  "SELECT DISTINCT id, username, timestamp FROM ways, way_tags WHERE id = way AND (TRUE"
#define SQL_XAPI_WAY_MID  " OR id IN (SELECT way FROM way_nds WHERE to_node IN ("
#define SQL_XAPI_WAY_END  "))) ORDER BY id"

#define SQL_XAPI_WAY_ID   "SELECT DISTINCT id FROM ways, way_tags WHERE id = way AND (TRUE"
#define SQL_XAPI_WAY_ID_END ")))"

#define SQL_XAPI_WAY_WT     "SELECT way, k, v FROM way_tags WHERE way IN ("
#define SQL_XAPI_WAY_WT_END ") ORDER BY way"

#define SQL_XAPI_WAY_WN     "SELECT way, to_node FROM way_nds WHERE way IN ("
#define SQL_XAPI_WAY_WN_END ") ORDER BY way, idx"


#define SQL_XAPI_REL      "SELECT DISTINCT id, username, timestamp FROM relations, relation_tags WHERE id = relation AND (TRUE"
#define SQL_XAPI_REL_MID  " OR id IN (SELECT relation FROM relation_members_node WHERE to_node IN ("
#define SQL_XAPI_REL_END  "))) ORDER BY id"

#define SQL_XAPI_REL_ID	  "SELECT DISTINCT id FROM relations, relation_tags WHERE id = relation AND (TRUE"
#define SQL_XAPI_REL_ID_END ")))"

#define SQL_XAPI_REL_RT     "SELECT relation, k, v FROM relation_tags WHERE relation IN ("
#define SQL_XAPI_REL_RT_END ") ORDER BY relation"
#define SQL_XAPI_REL_RR	    "SELECT relation, to_relation, role FROM relation_members_relation WHERE relation IN ("
#define SQL_XAPI_REL_RR_END ") ORDER BY relation, idx"
#define SQL_XAPI_REL_RN     "SELECT relation, to_node, role FROM relation_members_node WHERE relation IN ("
#define SQL_XAPI_REL_RN_END ") ORDER BY relation, idx"
#define SQL_XAPI_REL_RW     "SELECT relation, to_way, role FROM relation_members_way WHERE relation IN ("
#define SQL_XAPI_REL_RW_END ") ORDER BY relation, idx"


#define SQL_GET_OBJECT_BY_ID "SELECT id, long, lat, username, timestamp FROM " SQL_NODES " WHERE id = %lu", id
#define SQL_GET_OBJECT_BY_IDS "SELECT id, long, lat, username, timestamp FROM " SQL_NODES " WHERE id IN (%s) ORDER BY id"
#define SQL_GET_OBJECT_BY_WAY "SELECT id, long, lat, username, timestamp FROM " SQL_NODES " WHERE id IN (SELECT DISTINCT to_node FROM way_nds WHERE way = %lu) ORDER BY id", id
#define SQL_GET_OBJECT_BY_RELATION "SELECT id, long, lat, username, timestamp FROM " SQL_NODES " WHERE id IN (SELECT DISTINCT to_node FROM relation_members_node WHERE relation = %lu) ORDER BY id", id

#define SQL_GET_NODES_BY_RELATION "SELECT DISTINCT id, long, lat, username, timestamp FROM " SQL_NODES " WHERE id IN (SELECT to_node FROM relation_members_node WHERE relation = %lu) OR id IN (SELECT to_node FROM way_nds WHERE way IN (SELECT to_way FROM relation_members_way WHERE relation = %lu)) ORDER BY id;", id, id

#define SQL_GET_OBJTAG_BY_ID "SELECT node, k, v FROM node_tags WHERE node = %lu", id
#define SQL_GET_OBJTAG_BY_IDS "SELECT node, k, v FROM node_tags WHERE node IN (%s) ORDER BY node"
#define SQL_GET_OBJTAG_BY_WAY "SELECT node, k, v FROM node_tags WHERE node IN (SELECT DISTINCT to_node FROM way_nds WHERE way = %lu) ORDER BY node", id
#define SQL_GET_OBJTAG_BY_RELATION "SELECT node, k, v FROM node_tags WHERE node IN (SELECT DISTINCT to_node FROM relation_members_node WHERE relation = %lu) ORDER BY node", id

#define SQL_GET_NODES_TAGS_BY_RELATION "SELECT DISTINCT node, k, v FROM node_tags WHERE node IN (SELECT to_node FROM relation_members_node WHERE relation = %lu) OR id IN (SELECT to_node FROM way_nds WHERE way IN (SELECT to_way FROM relation_members_way WHERE relation = %lu)) ORDER BY node;", id, id

static ret_t result_way_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *ways, MapiHdl *way_tags, MapiHdl *way_nds, cherokee_buffer_t *buf);

static ret_t result_nodenew_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *nodes, MapiHdl *node_tags, cherokee_buffer_t *buf) {
	if (mapi_get_row_count(*nodes) != 0) {
		unsigned long int current_tag = 0;

		while (mapi_fetch_row(*nodes)) {
			unsigned long int current_node = strtoul(mapi_fetch_field(*nodes, 0), (char **) NULL, 10);
			unsigned long len;

			cherokee_buffer_add_va (buf, XML(NODE, "  "), mapi_fetch_field(*nodes, 0),
							 	      mapi_fetch_field(*nodes, 1),
								      mapi_fetch_field(*nodes, 2),
								      mapi_fetch_field(*nodes, 3),
								      mapi_fetch_field(*nodes, 4));

			len = buf->len;

			while (current_tag < current_node) {
				if (mapi_fetch_row(*node_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*node_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_tag == current_node) {
				const char * key = mapi_fetch_field(*node_tags, 1);
				const char * value = mapi_fetch_field(*node_tags, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(TAG, "    ") "/", key, (value == NULL ? "" : value));

				if (mapi_fetch_row(*node_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*node_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			if (buf->len > len)
				cherokee_buffer_add_str (buf, ">" CRLF XMLCLOSE(NODE, "  "));
			else
				cherokee_buffer_add_str (buf, XMLCLOSESHORT);
		}
	}

	return ret_ok;
}

#define SQL_GET_WAY_BY_ND "SELECT id, username, timestamp FROM ways WHERE id IN (SELECT DISTINCT way FROM way_nds WHERE to_node = %lu) ORDER BY id", id
#define SQL_GET_WAY_BY_RELATION "SELECT id, username, timestamp FROM ways WHERE id IN (SELECT DISTINCT to_way FROM relation_members_way WHERE relation = %lu) ORDER BY id", id

#define SQL_GET_WAY_TAGS_BY_ND "SELECT way, k, v FROM way_tags WHERE way IN (SELECT DISTINCT way FROM way_nds WHERE to_node = %lu) ORDER BY way", id
#define SQL_GET_WAY_TAGS_BY_RELATION "SELECT way, k, v FROM way_tags WHERE way IN (SELECT DISTINCT to_way FROM relation_members_way WHERE relation = %lu) ORDER BY way", id

#define SQL_GET_WAY_NDS_BY_ND "SELECT way, to_node FROM way_nds WHERE way IN (SELECT DISTINCT way FROM way_nds WHERE to_node = %lu) ORDER BY way, to_node", id
#define SQL_GET_WAY_NDS_BY_RELATION "SELECT way, to_node FROM way_nds WHERE way IN (SELECT DISTINCT to_way FROM relation_members_way WHERE relation = %lu) ORDER BY way, to_node, idx", id

#define SQL_GET_WAY_BY_NDS "SELECT id, username, timestamp FROM ways WHERE id IN (SELECT DISTINCT way FROM way_nds WHERE to_node IN (%s)) ORDER BY id"
#define SQL_GET_WAY_TAGS_BY_NDS "SELECT way, k, v FROM way_tags WHERE way IN (SELECT DISTINCT way FROM way_nds WHERE to_node IN (%s)) ORDER BY way"
#define SQL_GET_WAY_NDS_BY_NDS "SELECT way, to_node FROM way_nds WHERE AND way IN (SELECT DISTINCT way FROM way_nds WHERE to_node IN (%s)) ORDER BY way, to_node, idx"

/*
static ret_t result_nodenew_ways_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *nodes, MapiHdl *node_tags, cherokee_buffer_t *buf) {
	if (mapi_get_row_count(*nodes) != 0) {
		unsigned long int current_tag = 0;
		cherokee_buffer_t nds = CHEROKEE_BUF_INIT;

		while (mapi_fetch_row(*nodes)) {
			const char* id = mapi_fetch_field(*nodes, 0);
			unsigned long int current_node = strtoul(mapi_fetch_field(*nodes, 0), (char **) NULL, 10);
			unsigned long len;

			cherokee_buffer_add(&nds, id, strlen(id));
			cherokee_buffer_add_str(&nds, ", ");

			cherokee_buffer_add_va (buf, XML(NODE, "  "), id,
							 	      mapi_fetch_field(*nodes, 1),
								      mapi_fetch_field(*nodes, 2),
								      mapi_fetch_field(*nodes, 3),
								      mapi_fetch_field(*nodes, 4));

			len = buf->len;

			while (current_tag < current_node) {
				if (mapi_fetch_row(*node_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*node_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_tag == current_node) {
				const char * key = mapi_fetch_field(*node_tags, 1);
				const char * value = mapi_fetch_field(*node_tags, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(TAG, "    ") "/", key, (value == NULL ? "" : value));

				if (mapi_fetch_row(*node_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*node_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			if (buf->len > len)
				cherokee_buffer_add_str (buf, ">" CRLF XMLCLOSE(NODE, "  "));
			else
				cherokee_buffer_add_str (buf, XMLCLOSESHORT);
		}

		if (nds.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&nds, 2);
			cherokee_buffer_add_va (&sql1, SQL_GET_WAY_BY_NDS, nds.buf);
			cherokee_buffer_add_va (&sql2, SQL_GET_WAY_TAGS_BY_NDS, nds.buf);
			cherokee_buffer_add_va (&sql3, SQL_GET_WAY_NDS_BY_NDS, nds.buf);
			run_sql3(hdl, &sql1, &sql2, &sql3, buf, result_way_to_xml);	
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			cherokee_buffer_mrproper(&sql3);
		}

		cherokee_buffer_mrproper(&nds);
	}

	return ret_ok;
}
*/

#define SQL_GET_WAY_BY_ID       "SELECT id, username, timestamp FROM ways WHERE id = %lu", id
#define SQL_GET_WAY_BY_IDS      "SELECT id, username, timestamp FROM ways WHERE id IN (%s) ORDER BY id"
#define SQL_GET_WAY_TAGS_BY_ID  "SELECT way, k, v FROM way_tags WHERE way = %lu", id
#define SQL_GET_WAY_TAGS_BY_IDS "SELECT way, k, v FROM way_tags WHERE way IN (%s) ORDER BY way"
#define SQL_GET_WAY_NDS_BY_ID   "SELECT way, to_node FROM way_nds WHERE way = %lu ORDER BY idx", id
#define SQL_GET_WAY_NDS_BY_IDS  "SELECT way, to_node FROM way_nds WHERE way IN (%s) ORDER BY way, idx"

static ret_t result_way_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *ways, MapiHdl *way_tags,
	MapiHdl *way_nds, cherokee_buffer_t *buf) {

	if (mapi_get_row_count(*ways) != 0) {
		unsigned long int current_tag = 0, current_member_node = 0;

		while (mapi_fetch_row(*ways)) {
			unsigned long int current_way = strtoul(mapi_fetch_field(*ways, 0), (char **) NULL, 10);
			unsigned long len;

			cherokee_buffer_add_va (buf, XML(WAY, "  "), mapi_fetch_field(*ways, 0),
								     mapi_fetch_field(*ways, 1),
								     mapi_fetch_field(*ways, 2));

			len = buf->len;

			while (current_tag < current_way) {
				if (mapi_fetch_row(*way_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*way_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_tag == current_way) {
				const char * key = mapi_fetch_field(*way_tags, 1);
				const char * value = mapi_fetch_field(*way_tags, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(TAG, "    ") "/", key, (value == NULL ? "" : value));

				if (mapi_fetch_row(*way_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*way_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_member_node < current_way) {
				if (mapi_fetch_row(*way_nds) != 0) 
					current_member_node = strtoul(mapi_fetch_field(*way_nds, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			while (current_member_node == current_way) {
				const char * ref = mapi_fetch_field(*way_nds, 1);
				cherokee_buffer_add_va (buf, ">" CRLF XML(ND, "    ") "/", ref);

				if (mapi_fetch_row(*way_nds) != 0)
					current_member_node = strtoul(mapi_fetch_field(*way_nds, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			if (buf->len > len)
				cherokee_buffer_add_str (buf, ">" CRLF XMLCLOSE(WAY, "  "));
			else
				cherokee_buffer_add_str (buf, XMLCLOSESHORT);
		}
	}

	return ret_ok;
}

#if MATERIALIZE
static ret_t result_way_full_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *ways, MapiHdl *way_tags,
	MapiHdl *way_nds, cherokee_buffer_t *buf) {

	if (mapi_get_row_count(*ways) != 0) {
		unsigned long int current_tag = 0, current_member_node = 0;
		cherokee_buffer_t nds = CHEROKEE_BUF_INIT;

		while (mapi_fetch_row(*ways)) {
			unsigned long int current_way = strtoul(mapi_fetch_field(*ways, 0), (char **) NULL, 10);
			unsigned long len;

			cherokee_buffer_add_va (buf, XML(WAY, "  "), mapi_fetch_field(*ways, 0),
								     mapi_fetch_field(*ways, 1),
								     mapi_fetch_field(*ways, 2));

			len = buf->len;

			while (current_tag < current_way) {
				if (mapi_fetch_row(*way_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*way_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_tag == current_way) {
				const char * key = mapi_fetch_field(*way_tags, 1);
				const char * value = mapi_fetch_field(*way_tags, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(TAG, "    ") "/", key, (value == NULL ? "" : value));

				if (mapi_fetch_row(*way_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*way_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_member_node < current_way) {
				if (mapi_fetch_row(*way_nds) != 0) 
					current_member_node = strtoul(mapi_fetch_field(*way_nds, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			while (current_member_node == current_way) {
				const char * ref = mapi_fetch_field(*way_nds, 1);
				cherokee_buffer_add_va (buf, ">" CRLF XML(ND, "    ") "/", ref);
				cherokee_buffer_add (&nds, ref, strlen(ref));
				cherokee_buffer_add_str (&nds, ", ");

				if (mapi_fetch_row(*way_nds) != 0)
					current_member_node = strtoul(mapi_fetch_field(*way_nds, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			if (buf->len > len)
				cherokee_buffer_add_str (buf, ">" CRLF XMLCLOSE(WAY, "  "));
			else
				cherokee_buffer_add_str (buf, XMLCLOSESHORT);
		}

		if (nds.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&nds, 2);
			cherokee_buffer_add_va (&sql1, SQL_GET_OBJECT_BY_IDS, nds.buf);
			cherokee_buffer_add_va (&sql2, SQL_GET_OBJTAG_BY_IDS, nds.buf);
			run_sql2(hdl, &sql1, &sql2, buf, result_nodenew_to_xml);	
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
		}

		cherokee_buffer_mrproper(&nds);
	}

	return ret_ok;
}
#endif


/* RELATION */

#define SQL_GET_RELATION_BY_ND  "SELECT DISTINCT relation FROM relation_members_node WHERE to_node = %lu", id
#define SQL_GET_RELATION_BY_WAY "SELECT DISTINCT relation FROM relation_members_way WHERE to_way = %lu", id
#define SQL_GET_RELATION_BY_REL "SELECT DISTINCT relation FROM relation_members_relation WHERE to_relation = %lu", id

#if MATERIALIZE
static ret_t result_id_to_list(cherokee_handler_osm_t *hdl, MapiHdl *thingie, cherokee_buffer_t *buf) {
	unsigned long int len = buf->len;
	if (mapi_get_row_count(*thingie) != 0) {
		while (mapi_fetch_row(*thingie)) {
			const char *id = mapi_fetch_field(*thingie, 0);
			cherokee_buffer_add (buf, id, strlen(id));
			cherokee_buffer_add_str (buf, ", ");
		}
	}
	if (buf->len > len) {
		cherokee_buffer_drop_ending(buf, 2);
	}
	
	return ret_ok;
}
#endif


#define SQL_GET_RELATION_BY_ID "SELECT id, username, timestamp FROM relations WHERE id = %lu", id
#define SQL_GET_RELATION_BY_IDS "SELECT id, username, timestamp FROM relations WHERE id IN (%s) ORDER BY id"
#define SQL_GET_RELATION_BY_RELATION "SELECT id, username, timestamp FROM relations WHERE id IN (SELECT DISTINCT relation FROM relation_members_node WHERE to_node = %lu) ORDER BY id", id
#define SQL_GET_RELATION_TAGS_BY_ID "SELECT relation, k, v FROM relation_tags WHERE relation = %lu", id
#define SQL_GET_RELATION_TAGS_BY_IDS "SELECT relation, k, v FROM relation_tags WHERE relation IN (%s) ORDER BY relation"
#define SQL_GET_RELATION_TAGS_BY_RELATION "SELECT relation, k, v FROM relation_tags WHERE relation IN (SELECT DISTINCT relation FROM relation_members_node WHERE to_node = %lu) ORDER BY relation", id
#define SQL_GET_RELATION_MEMBER_NODE_BY_ID "SELECT relation, to_node, role FROM relation_members_node WHERE relation = %lu ORDER BY idx", id
#define SQL_GET_RELATION_MEMBER_NODE_BY_IDS "SELECT relation, to_node, role FROM relation_members_node WHERE relation IN (%s) ORDER BY relation, idx"
#define SQL_GET_RELATION_MEMBER_NODE_BY_RELATION "SELECT relation, to_node, role FROM relation_members_node WHERE relation IN (SELECT DISTINCT relation FROM relation_members_node WHERE to_node = %lu) ORDER BY relation", id
#define SQL_GET_RELATION_MEMBER_RELATION_BY_ID "SELECT relation, to_relation, role FROM relation_members_relation WHERE relation = %lu ORDER BY idx", id
#define SQL_GET_RELATION_MEMBER_RELATION_BY_IDS "SELECT relation, to_relation, role FROM relation_members_relation WHERE relation IN (%s) ORDER BY relation, idx"
#define SQL_GET_RELATION_MEMBER_RELATION_BY_RELATION "SELECT relation, to_relation, role FROM relation_members_relation WHERE relation IN (SELECT DISTINCT relation FROM relation_members_relation WHERE to_relation = %lu) ORDER BY relation", id
#define SQL_GET_RELATION_MEMBER_WAY_BY_ID "SELECT relation, to_way, role FROM relation_members_way WHERE relation = %lu ORDER BY idx", id
#define SQL_GET_RELATION_MEMBER_WAY_BY_IDS "SELECT relation, to_way, role FROM relation_members_way WHERE relation IN (%s) ORDER BY relation, idx"
#define SQL_GET_RELATION_MEMBER_WAY_BY_RELATION "SELECT relation, to_way, role FROM relation_members_way WHERE relation IN (SELECT DISTINCT relation FROM relation_members_way WHERE to_way = %lu) ORDER BY relation", id


static ret_t result_relation_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *relations, MapiHdl *relation_tags,
	MapiHdl *relation_mn, MapiHdl *relation_mr, MapiHdl *relation_mw, cherokee_buffer_t *buf) {

	if (mapi_get_row_count(*relations) != 0) {
		unsigned long int current_tag = 0, current_member_node = 0, current_member_relation = 0, current_member_way = 0;

		while (mapi_fetch_row(*relations)) {
			unsigned long int current_relation = strtoul(mapi_fetch_field(*relations, 0), (char **) NULL, 10);
			unsigned long len;

			cherokee_buffer_add_va (buf, XML(RELATION, "  "), mapi_fetch_field(*relations, 0),
								          mapi_fetch_field(*relations, 1),
								          mapi_fetch_field(*relations, 2));

			len = buf->len;

			while (current_tag < current_relation) {
				if (mapi_fetch_row(*relation_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*relation_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_tag == current_relation) {
				const char * key = mapi_fetch_field(*relation_tags, 1);
				const char * value = mapi_fetch_field(*relation_tags, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(TAG, "    ") "/", key, (value == NULL ? "" : value));

				if (mapi_fetch_row(*relation_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*relation_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_member_node < current_relation) {
				if (mapi_fetch_row(*relation_mn) != 0) 
					current_member_node = strtoul(mapi_fetch_field(*relation_mn, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			while (current_member_node == current_relation) {
				const char *to_node = mapi_fetch_field(*relation_mn, 1);
				const char *role = mapi_fetch_field(*relation_mn, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(MEMBER, "    ") "/", MEMBER_TYPE_NODE, (to_node ? to_node : ""), (role ? role : ""));

				if (mapi_fetch_row(*relation_mn) != 0)
					current_member_node = strtoul(mapi_fetch_field(*relation_mn, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			while (current_member_relation < current_relation) {
				if (mapi_fetch_row(*relation_mr) != 0) 
					current_member_relation = strtoul(mapi_fetch_field(*relation_mr, 0), (char **) NULL, 10);
				else 
					current_member_relation = ULONG_MAX;
			}

			while (current_member_relation == current_relation) {
				const char *to_relation = mapi_fetch_field(*relation_mr, 1);
				const char *role = mapi_fetch_field(*relation_mr, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(MEMBER, "    ") "/", MEMBER_TYPE_RELATION, (to_relation ? to_relation : ""), (role ? role : ""));

				if (mapi_fetch_row(*relation_mr) != 0)
					current_member_relation = strtoul(mapi_fetch_field(*relation_mr, 0), (char **) NULL, 10);
				else 
					current_member_relation = ULONG_MAX;
			}

			while (current_member_way < current_relation) {
				if (mapi_fetch_row(*relation_mw) != 0) 
					current_member_way = strtoul(mapi_fetch_field(*relation_mw, 0), (char **) NULL, 10);
				else 
					current_member_way = ULONG_MAX;
			}

			while (current_member_way == current_relation) {
				const char *to_way = mapi_fetch_field(*relation_mw, 1);
				const char *role = mapi_fetch_field(*relation_mw, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(MEMBER, "    ") "/", MEMBER_TYPE_WAY, (to_way ? to_way : ""), (role ? role : ""));

				if (mapi_fetch_row(*relation_mw) != 0)
					current_member_way = strtoul(mapi_fetch_field(*relation_mw, 0), (char **) NULL, 10);
				else 
					current_member_way = ULONG_MAX;
			}

			if (buf->len > len)
				cherokee_buffer_add_str (buf, ">" CRLF XMLCLOSE(RELATION, "  "));
			else
				cherokee_buffer_add_str (buf, XMLCLOSESHORT);
		}
	}

	return ret_ok;
}

#if MATERIALIZE
static ret_t result_relation_full_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *relations, MapiHdl *relation_tags,
	MapiHdl *relation_mn, MapiHdl *relation_mr, MapiHdl *relation_mw, cherokee_buffer_t *buf) {

	if (mapi_get_row_count(*relations) != 0) {
		unsigned long int current_tag = 0, current_member_node = 0, current_member_relation = 0, current_member_way = 0;
		cherokee_buffer_t nds = CHEROKEE_BUF_INIT;
		cherokee_buffer_t wys = CHEROKEE_BUF_INIT;
		cherokee_buffer_t rls = CHEROKEE_BUF_INIT;

		while (mapi_fetch_row(*relations)) {
			unsigned long int current_relation = strtoul(mapi_fetch_field(*relations, 0), (char **) NULL, 10);
			unsigned long len;

			cherokee_buffer_add_va (buf, XML(RELATION, "  "), mapi_fetch_field(*relations, 0),
								          mapi_fetch_field(*relations, 1),
								          mapi_fetch_field(*relations, 2));

			len = buf->len;

			while (current_tag < current_relation) {
				if (mapi_fetch_row(*relation_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*relation_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_tag == current_relation) {
				const char * key = mapi_fetch_field(*relation_tags, 1);
				const char * value = mapi_fetch_field(*relation_tags, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(TAG, "    ") "/", key, (value == NULL ? "" : value));

				if (mapi_fetch_row(*relation_tags) != 0)
					current_tag = strtoul(mapi_fetch_field(*relation_tags, 0), (char **) NULL, 10);
				else
					current_tag = ULONG_MAX;
			}

			while (current_member_node < current_relation) {
				if (mapi_fetch_row(*relation_mn) != 0) 
					current_member_node = strtoul(mapi_fetch_field(*relation_mn, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			while (current_member_node == current_relation) {
				const char *to_node = mapi_fetch_field(*relation_mn, 1);
				const char *role = mapi_fetch_field(*relation_mn, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(MEMBER, "    ") "/", MEMBER_TYPE_NODE, (to_node ? to_node : ""), (role ? role : ""));
                                
				cherokee_buffer_add (&nds, to_node, strlen(to_node));
				cherokee_buffer_add_str (&nds, ", ");

				if (mapi_fetch_row(*relation_mn) != 0)
					current_member_node = strtoul(mapi_fetch_field(*relation_mn, 0), (char **) NULL, 10);
				else 
					current_member_node = ULONG_MAX;
			}

			while (current_member_relation < current_relation) {
				if (mapi_fetch_row(*relation_mr) != 0) 
					current_member_relation = strtoul(mapi_fetch_field(*relation_mr, 0), (char **) NULL, 10);
				else 
					current_member_relation = ULONG_MAX;
			}

			while (current_member_relation == current_relation) {				
				const char *to_relation = mapi_fetch_field(*relation_mr, 1);
				const char *role = mapi_fetch_field(*relation_mr, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(MEMBER, "    ") "/", MEMBER_TYPE_RELATION, (to_relation ? to_relation : ""), (role ? role : ""));
				
				cherokee_buffer_add (&rls, to_relation, strlen(to_relation));
				cherokee_buffer_add_str (&rls, ", ");

				if (mapi_fetch_row(*relation_mr) != 0)
					current_member_relation = strtoul(mapi_fetch_field(*relation_mr, 0), (char **) NULL, 10);
				else 
					current_member_relation = ULONG_MAX;
			}

			while (current_member_way < current_relation) {
				if (mapi_fetch_row(*relation_mw) != 0) 
					current_member_way = strtoul(mapi_fetch_field(*relation_mw, 0), (char **) NULL, 10);
				else 
					current_member_way = ULONG_MAX;
			}

			while (current_member_way == current_relation) {
				const char *to_way = mapi_fetch_field(*relation_mw, 1);
				const char *role = mapi_fetch_field(*relation_mw, 2);
				cherokee_buffer_add_va (buf, ">" CRLF XML(MEMBER, "    ") "/", MEMBER_TYPE_WAY, (to_way ? to_way : ""), (role ? role : ""));
				
				cherokee_buffer_add (&wys, to_way, strlen(to_way));
				cherokee_buffer_add_str (&wys, ", ");

				if (mapi_fetch_row(*relation_mw) != 0)
					current_member_way = strtoul(mapi_fetch_field(*relation_mw, 0), (char **) NULL, 10);
				else 
					current_member_way = ULONG_MAX;
			}

			if (buf->len > len)
				cherokee_buffer_add_str (buf, ">" CRLF XMLCLOSE(RELATION, "  "));
			else
				cherokee_buffer_add_str (buf, XMLCLOSESHORT);
		}
		
		if (nds.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&nds, 2);
			cherokee_buffer_add_va (&sql1, SQL_GET_OBJECT_BY_IDS, nds.buf);
			cherokee_buffer_add_va (&sql2, SQL_GET_OBJTAG_BY_IDS, nds.buf);
			run_sql2(hdl, &sql1, &sql2, buf, result_nodenew_to_xml);	
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
		}
		
		if (rls.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql4 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql5 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&rls, 2);
			cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_IDS, rls.buf);
			cherokee_buffer_add_va (&sql2, SQL_GET_RELATION_TAGS_BY_IDS, rls.buf);
			cherokee_buffer_add_va (&sql3, SQL_GET_RELATION_MEMBER_NODE_BY_IDS, rls.buf);
			cherokee_buffer_add_va (&sql4, SQL_GET_RELATION_MEMBER_RELATION_BY_IDS, rls.buf);
			cherokee_buffer_add_va (&sql5, SQL_GET_RELATION_MEMBER_WAY_BY_IDS, rls.buf);
			run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);	
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			cherokee_buffer_mrproper(&sql3);
			cherokee_buffer_mrproper(&sql4);
			cherokee_buffer_mrproper(&sql5);
		}
		
		if (wys.len > 0) {
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;
			cherokee_buffer_drop_ending(&wys, 2);
			cherokee_buffer_add_va (&sql1, SQL_GET_WAY_BY_IDS, wys.buf);
			cherokee_buffer_add_va (&sql2, SQL_GET_WAY_TAGS_BY_IDS, wys.buf);
			cherokee_buffer_add_va (&sql3, SQL_GET_WAY_NDS_BY_IDS, wys.buf);
			run_sql3(hdl, &sql1, &sql2, &sql3, buf, result_way_to_xml);	
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			cherokee_buffer_mrproper(&sql3);
		}

		cherokee_buffer_mrproper(&nds);
		cherokee_buffer_mrproper(&rls);
		cherokee_buffer_mrproper(&wys);

	}

	return ret_ok;
}
#endif

/*
static ret_t result_relationway_to_xml(cherokee_handler_osm_t *hdl, MapiHdl *relations, MapiHdl *relation_tags,
		MapiHdl *members_relation, MapiHdl *members_node,
		cherokee_buffer_t *buf) {
	unsigned long int current_tag = 0, current_member_node = 0, current_member_relation = 0;

	enum WayRelation { RELATION_TYPE = 0, WAY_TYPE };
	enum WayRelation type;

	while (mapi_fetch_row(*relations)) {
		cherokee_buffer_t tag = CHEROKEE_BUF_INIT;
		type = RELATION_TYPE;
		short int touched = 0;
		unsigned long int current_relation = strtoul(mapi_fetch_field(*relations, 0), (char **) NULL, 10);

		while (current_tag < current_relation) {
			if (mapi_fetch_row(*relation_tags) != 0)
				current_tag = strtoul(mapi_fetch_field(*relation_tags, 0), (char **) NULL, 10);
			else
				current_tag = ULONG_MAX;
		}

		while (current_tag == current_relation) {
			const char * key = mapi_fetch_field(*relation_tags, 1);
			const char * value = mapi_fetch_field(*relation_tags, 2);

			if (key && value && strcmp(key, "type") == 0 && strcmp(value, "way") == 0)
				type = WAY_TYPE;
			else {
				cherokee_buffer_add_va (&tag, XML(TAG, "    ") XMLCLOSESHORT, key, (value == NULL ? "" : value));
				touched = 1;
			}

			if (mapi_fetch_row(*relation_tags) != 0)
				current_tag = strtoul(mapi_fetch_field(*relation_tags, 0), (char **) NULL, 10);
			else
				current_tag = ULONG_MAX;
		}
		if (type == RELATION_TYPE) {
			cherokee_buffer_add_va (buf, XML(RELATION, "  "), mapi_fetch_field(*relations, 0),
					mapi_fetch_field(*relations, 1),
					mapi_fetch_field(*relations, 2) );
		} else {
			cherokee_buffer_add_va (buf, XML(WAY, "  "),      mapi_fetch_field(*relations, 0),
					mapi_fetch_field(*relations, 1),
					mapi_fetch_field(*relations, 2) );
		}

		if (touched == 1) {
			cherokee_buffer_add_str (buf, XMLCONTINUE);
			cherokee_buffer_add_buffer (buf, &tag);
		}
		cherokee_buffer_mrproper(&tag);

		while (current_member_node < current_relation) {
			if (mapi_fetch_row(*members_node) != 0) 
				current_member_node = strtoul(mapi_fetch_field(*members_node, 0), (char **) NULL, 10);
			else 
				current_member_node = ULONG_MAX;
		}

		while (current_member_node == current_relation) {
			const char * to_node = mapi_fetch_field(*members_node, 1);

			if (touched == 0) {
				cherokee_buffer_add_str (buf, ">");
				touched = 1;
			}

			if (type == RELATION_TYPE) {
				const char * role = mapi_fetch_field(*members_node, 2);
				cherokee_buffer_add_va (buf, XML(MEMBER, "    ")XMLCLOSESHORT, MEMBER_TYPE_NODE, to_node, role);
			} else 
				cherokee_buffer_add_va (buf, XML(ND, "    ")XMLCLOSESHORT, to_node);

			if (mapi_fetch_row(*members_node) != 0)
				current_member_node = strtoul(mapi_fetch_field(*members_node, 0), (char **) NULL, 10);
			else 
				current_member_node = ULONG_MAX;
		}

		if (type == RELATION_TYPE) {
			while (current_member_relation < current_relation) {
				if (mapi_fetch_row(*members_relation) != 0)
					current_member_relation = strtoul(mapi_fetch_field(*members_relation, 0), (char **) NULL, 10);
				else 
					current_member_relation = ULONG_MAX;
			}


			while (current_member_relation == current_relation) {
				const char * to_relation = mapi_fetch_field(*members_relation, 1);

				if (touched == 0) {
					cherokee_buffer_add_str (buf, ">");
					touched = 1;
				}

				if (type == RELATION_TYPE) {
					const char * role = mapi_fetch_field(*members_relation, 2);
					const char * wayornot = mapi_fetch_field(*members_relation, 3);
					if (strcmp(wayornot, "true") == 0)
						cherokee_buffer_add_va (buf, XML(MEMBER, "    ")XMLCLOSESHORT, MEMBER_TYPE_WAY, to_relation, role);
					else
						cherokee_buffer_add_va (buf, XML(MEMBER, "    ")XMLCLOSESHORT, MEMBER_TYPE_RELATION, to_relation, role);
				} else 
					cherokee_buffer_add_va (buf, XML(ND, "  ")XMLCLOSESHORT, MEMBER_TYPE_NODE, to_relation);

				if (mapi_fetch_row(*members_relation) != 0)
					current_member_relation = strtoul(mapi_fetch_field(*members_relation, 0), (char **) NULL, 10);
				else 
					current_member_relation = ULONG_MAX;
			}
		}

		if (touched == 0) {
			cherokee_buffer_add_str (buf, XMLCLOSESHORT);
		} else if (type == RELATION_TYPE)
			cherokee_buffer_add_va (buf, "%s", XMLCLOSE(RELATION, "  "));
		else
			cherokee_buffer_add_va (buf, "%s", XMLCLOSE(WAY, "  "));
	}

	return ret_ok;
}
*/

ret_t
get_object_by_id(cherokee_handler_osm_t *hdl, unsigned long int id, cherokee_buffer_t *buf, osm_state_get_t state) {
	ret_t ret;
	cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
	cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;
	cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;
	cherokee_buffer_t sql4 = CHEROKEE_BUF_INIT;
	cherokee_buffer_t sql5 = CHEROKEE_BUF_INIT;

	switch (state) {
		case OSM_NODE_COMMAND:
			cherokee_buffer_add_va (&sql1, SQL_GET_OBJECT_BY_ID);
			cherokee_buffer_add_va (&sql2, SQL_GET_OBJTAG_BY_ID);
			ret = run_sql2(hdl, &sql1, &sql2, buf, result_nodenew_to_xml);
			break;
		case OSM_NODE_COMMAND_WAYS:
			cherokee_buffer_add_va (&sql1, SQL_GET_WAY_BY_ND);
			cherokee_buffer_add_va (&sql2, SQL_GET_WAY_TAGS_BY_ND);
			cherokee_buffer_add_va (&sql3, SQL_GET_WAY_NDS_BY_ND);
			ret = run_sql3(hdl, &sql1, &sql2, &sql3, buf, result_way_to_xml);
			break;
		case OSM_NODE_COMMAND_RELATIONS: {
			cherokee_buffer_t relations = CHEROKEE_BUF_INIT;
#ifdef MATERIALIZE
			cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_ND);
			run_sql(hdl, &sql1, &relations, result_id_to_list);
			cherokee_buffer_mrproper(&sql1);
#else
			cherokee_buffer_add_va (&relations, SQL_GET_RELATION_BY_ND);				
#endif

			if (relations.len > 0) {
				cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql2, SQL_GET_RELATION_TAGS_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql3, SQL_GET_RELATION_MEMBER_NODE_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql4, SQL_GET_RELATION_MEMBER_RELATION_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql5, SQL_GET_RELATION_MEMBER_WAY_BY_IDS, relations.buf);
			
				ret = run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);
			} else {
				ret = ret_ok;
			}
			cherokee_buffer_mrproper(&relations);
			break;
		}
		case OSM_WAY_COMMAND:
			cherokee_buffer_add_va (&sql1, SQL_GET_WAY_BY_ID);
			cherokee_buffer_add_va (&sql2, SQL_GET_WAY_TAGS_BY_ID);
			cherokee_buffer_add_va (&sql3, SQL_GET_WAY_NDS_BY_ID);
			ret = run_sql3(hdl, &sql1, &sql2, &sql3, buf, result_way_to_xml);
			break;
		case OSM_WAY_COMMAND_FULL:
			cherokee_buffer_add_va (&sql1, SQL_GET_WAY_BY_ID);
			cherokee_buffer_add_va (&sql2, SQL_GET_WAY_TAGS_BY_ID);
			cherokee_buffer_add_va (&sql3, SQL_GET_WAY_NDS_BY_ID);
#ifdef MATERIALIZE
			ret = run_sql3(hdl, &sql1, &sql2, &sql3, buf, result_way_full_to_xml);
#else
			ret = run_sql3(hdl, &sql1, &sql2, &sql3, buf, result_way_to_xml);

			cherokee_buffer_add_va (&sql4, SQL_GET_OBJECT_BY_WAY);
			cherokee_buffer_add_va (&sql5, SQL_GET_OBJTAG_BY_WAY);
			ret = run_sql2(hdl, &sql4, &sql5, buf, result_nodenew_to_xml);
#endif
			break;
		case OSM_WAY_COMMAND_RELATIONS: {
			cherokee_buffer_t relations = CHEROKEE_BUF_INIT;
#ifdef MATERIALIZE
			cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_WAY);
			run_sql(hdl, &sql1, &relations, result_id_to_list);
			cherokee_buffer_mrproper(&sql1);
#else
			cherokee_buffer_add_va (&relations, SQL_GET_RELATION_BY_WAY);
#endif

			if (relations.len > 0) {
				cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql2, SQL_GET_RELATION_TAGS_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql3, SQL_GET_RELATION_MEMBER_NODE_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql4, SQL_GET_RELATION_MEMBER_RELATION_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql5, SQL_GET_RELATION_MEMBER_WAY_BY_IDS, relations.buf);

				ret = run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);
			} else {
				ret = ret_ok;
			}
			cherokee_buffer_mrproper(&relations);
			break;
		}
		case OSM_RELATION_COMMAND:
			cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_ID);
			cherokee_buffer_add_va (&sql2, SQL_GET_RELATION_TAGS_BY_ID);
			cherokee_buffer_add_va (&sql3, SQL_GET_RELATION_MEMBER_NODE_BY_ID);
			cherokee_buffer_add_va (&sql4, SQL_GET_RELATION_MEMBER_RELATION_BY_ID);
			cherokee_buffer_add_va (&sql5, SQL_GET_RELATION_MEMBER_WAY_BY_ID);
			ret = run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);
			break;
		case OSM_RELATION_COMMAND_FULL:
			cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_ID);
			cherokee_buffer_add_va (&sql2, SQL_GET_RELATION_TAGS_BY_ID);
			cherokee_buffer_add_va (&sql3, SQL_GET_RELATION_MEMBER_NODE_BY_ID);
			cherokee_buffer_add_va (&sql4, SQL_GET_RELATION_MEMBER_RELATION_BY_ID);
			cherokee_buffer_add_va (&sql5, SQL_GET_RELATION_MEMBER_WAY_BY_ID);
#ifdef MATERIALIZE
			ret = run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_full_to_xml);
#else
			ret = run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);
			
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			cherokee_buffer_mrproper(&sql3);
			cherokee_buffer_mrproper(&sql4);
			cherokee_buffer_mrproper(&sql5);
			
			cherokee_buffer_add_va (&sql1, SQL_GET_NODES_BY_RELATION);
			cherokee_buffer_add_va (&sql2, SQL_GET_NODES_TAGS_BY_RELATION);
			ret = run_sql2(hdl, &sql1, &sql1, buf, result_nodenew_to_xml);
			
			cherokee_buffer_add_va (&sql3, SQL_GET_WAY_BY_RELATION);
			cherokee_buffer_add_va (&sql4, SQL_GET_WAY_TAGS_BY_RELATION);
			cherokee_buffer_add_va (&sql5, SQL_GET_WAY_NDS_BY_RELATION);
			ret = run_sql3(hdl, &sql3, &sql4, &sql5, buf, result_way_to_xml);

			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			cherokee_buffer_mrproper(&sql3);
			cherokee_buffer_mrproper(&sql4);
			cherokee_buffer_mrproper(&sql5);

			cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_RELATION);
			cherokee_buffer_add_va (&sql2, SQL_GET_RELATION_TAGS_BY_RELATION);
			cherokee_buffer_add_va (&sql3, SQL_GET_RELATION_MEMBER_NODE_BY_RELATION);
			cherokee_buffer_add_va (&sql4, SQL_GET_RELATION_MEMBER_RELATION_BY_RELATION);
			cherokee_buffer_add_va (&sql5, SQL_GET_RELATION_MEMBER_WAY_BY_RELATION);
			ret = run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);
#endif
			break;
		case OSM_RELATION_COMMAND_RELATIONS: {
			cherokee_buffer_t relations = CHEROKEE_BUF_INIT;
#ifdef MATERIALIZE
			cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_REL);
			run_sql(hdl, &sql1, &relations, result_id_to_list);
			cherokee_buffer_mrproper(&sql1);
#else
			cherokee_buffer_add_va (&relations, SQL_GET_RELATION_BY_REL);
#endif

			if (relations.len > 0) {
				cherokee_buffer_add_va (&sql1, SQL_GET_RELATION_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql2, SQL_GET_RELATION_TAGS_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql3, SQL_GET_RELATION_MEMBER_NODE_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql4, SQL_GET_RELATION_MEMBER_RELATION_BY_IDS, relations.buf);
				cherokee_buffer_add_va (&sql5, SQL_GET_RELATION_MEMBER_WAY_BY_IDS, relations.buf);

				ret = run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);
			} else {
				ret = ret_ok;
			}
			cherokee_buffer_mrproper(&relations);
			break;
		}
		default:
			ret = ret_error;
	}
	cherokee_buffer_mrproper(&sql5);
	cherokee_buffer_mrproper(&sql4);
	cherokee_buffer_mrproper(&sql3);
	cherokee_buffer_mrproper(&sql2);
	cherokee_buffer_mrproper(&sql1);
	return ret;
}

static ret_t fetch_bbox(char *string, double *left, double *bottom, double *right, double *top) {
	char *token;
	if ((token = (char *) strsep( &string , ",")) != NULL) {
		*top = strtod(token, (char **) NULL);
		if (errno != ERANGE && (token = (char *) strsep( &string , ",")) != NULL) {
			*left = strtod(token, (char **) NULL);
			if (errno != ERANGE && (token = (char *) strsep( &string , ",")) != NULL) {
				*bottom = strtod(token, (char **) NULL);
				if (errno != ERANGE && (token = (char *) strsep( &string , ",")) != NULL) {
					*right = strtod(token, (char **) NULL);
					if (errno != ERANGE) {
						return ret_ok;
					}
				}
			}
		}
	}
	return ret_error;
}

#define SQL_PARSE_MAP_NODE_BBOX "SELECT id, long, lat, username, timestamp FROM " SQL_NODES " WHERE id IN (SELECT to_node FROM relation_members_node WHERE relation IN (SELECT relation FROM relation_members_node WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) OR id IN (SELECT to_node FROM way_nds WHERE way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) OR " SQL_BY_BBOX " ORDER BY id", BBOX_VA_ARGS, BBOX_VA_ARGS, BBOX_VA_ARGS

#define SQL_PARSE_MAP_NTAG_BBOX "SELECT node, k, v FROM node_tags WHERE node IN (SELECT id FROM " SQL_NODES " WHERE id IN (SELECT to_node FROM relation_members_node WHERE relation IN (SELECT relation FROM relation_members_node WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) OR id IN (SELECT to_node FROM way_nds WHERE way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) OR " SQL_BY_BBOX ") ORDER BY node", BBOX_VA_ARGS, BBOX_VA_ARGS, BBOX_VA_ARGS

#define SQL_PARSE_MAP_RELATIONS_BBOX "SELECT DISTINCT id, username, timestamp FROM relations WHERE id IN (SELECT relation FROM relation_members_node WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) OR id IN (SELECT relation FROM relation_members_way WHERE to_way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) ORDER BY id", BBOX_VA_ARGS, BBOX_VA_ARGS
#define SQL_PARSE_MAP_RTAGS_BBOX "SELECT relation, k, v FROM relation_tags WHERE relation IN (SELECT relation FROM relation_members_node WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) OR relation IN (SELECT relation FROM relation_members_way WHERE to_way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) ORDER BY relation", BBOX_VA_ARGS, BBOX_VA_ARGS

#define SQL_PARSE_MAP_MEMBERSN_BBOX "SELECT relation, to_node, role FROM relation_members_node WHERE relation IN (SELECT relation FROM relation_members_node WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) OR relation IN (SELECT relation FROM relation_members_way WHERE to_way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) ORDER BY relation", BBOX_VA_ARGS, BBOX_VA_ARGS
#define SQL_PARSE_MAP_MEMBERSW_BBOX "SELECT relation, to_way, role FROM relation_members_way WHERE relation IN (SELECT relation FROM relation_members_node WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) OR relation IN (SELECT relation FROM relation_members_way WHERE to_way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX "))) ORDER BY relation", BBOX_VA_ARGS, BBOX_VA_ARGS
#define SQL_PARSE_MAP_MEMBERSR_BBOX "SELECT relation, to_relation, role FROM relation_members_relation WHERE relation IN (SELECT relation FROM relation_members_node WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) ORDER BY relation", BBOX_VA_ARGS

#define SQL_PARSE_MAP_WAYS_BBOX "SELECT id, username, timestamp FROM ways WHERE id IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) ORDER BY id", BBOX_VA_ARGS
#define SQL_PARSE_MAP_WTAGS_BBOX "SELECT way, k, v FROM way_tags WHERE way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) ORDER BY way", BBOX_VA_ARGS
#define SQL_PARSE_MAP_WNDS_BBOX "SELECT way, to_node FROM way_nds WHERE way IN (SELECT way FROM way_nds WHERE to_node IN (SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX ")) ORDER BY way", BBOX_VA_ARGS



static void parse_map2(cherokee_handler_osm_t *hdl, cherokee_buffer_t *buf) {
	void *param;
	double left, bottom, right, top;
	cherokee_avl_t *arguments = HANDLER_CONN(hdl)->arguments;

	if (ret_ok == cherokee_avl_get_ptr (arguments, "bbox", &param)) {

		if (ret_ok == fetch_bbox((char *) param, &left, &bottom, &right, &top)) {
			TRACE ("osmapi", "%s BBOX: %f %f %f %f\n", __func__, left, bottom, right, top);
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql4 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql5 = CHEROKEE_BUF_INIT;
			cherokee_buffer_add_va (&sql1, SQL_PARSE_MAP_NODE_BBOX);
			cherokee_buffer_add_va (&sql2, SQL_PARSE_MAP_NTAG_BBOX);
			run_sql2(hdl, &sql1, &sql2, buf, result_nodenew_to_xml);
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			
			cherokee_buffer_add_va (&sql1, SQL_PARSE_MAP_WAYS_BBOX);
			cherokee_buffer_add_va (&sql2, SQL_PARSE_MAP_WTAGS_BBOX);
			cherokee_buffer_add_va (&sql3, SQL_PARSE_MAP_WNDS_BBOX);
			run_sql3(hdl, &sql1, &sql2, &sql3, buf, result_way_to_xml);
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			cherokee_buffer_mrproper(&sql3);

			cherokee_buffer_add_va (&sql1, SQL_PARSE_MAP_RELATIONS_BBOX);
			cherokee_buffer_add_va (&sql2, SQL_PARSE_MAP_RTAGS_BBOX);
			cherokee_buffer_add_va (&sql3, SQL_PARSE_MAP_MEMBERSN_BBOX);
			cherokee_buffer_add_va (&sql4, SQL_PARSE_MAP_MEMBERSR_BBOX);
			cherokee_buffer_add_va (&sql5, SQL_PARSE_MAP_MEMBERSW_BBOX);
			run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, buf, result_relation_to_xml);
			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
			cherokee_buffer_mrproper(&sql3);
			cherokee_buffer_mrproper(&sql4);
			cherokee_buffer_mrproper(&sql5);
		}

	}
}


static void parse_nodes(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
}
static void parse_nodes_search(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
}

static void parse_relations(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
}
static void parse_relations_search(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
}

static void parse_ways(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
}
static void parse_ways_search(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
}
static void parse_trackpoints(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
	double left, bottom, right, top;
	unsigned long int page;
	void *param;
	printf("%s\n", __func__);

	if (ret_ok == cherokee_avl_get_ptr (arguments, "bbox", &param)) {

		if (ret_ok == fetch_bbox((char *) param, &left, &bottom, &right, &top)) {
			cherokee_buffer_add_va (buf, "%f %f %f %f", left, bottom, right, top);
		}

	}

	if (ret_ok == cherokee_avl_get_ptr (arguments, "page", &param)) {
		page = strtoul(param, (char **) NULL, 10);
		if (errno == ERANGE)
			page = 1;
	} else {
		page = 1;
	}
}

static void
changes_byhour(cherokee_buffer_t *buf, unsigned long int zoom, unsigned long int hours) {
	cherokee_buffer_add_va (buf, "%d %d", zoom, hours);
}

static void
changes_bystartend(cherokee_buffer_t *buf, unsigned long int zoom, char *start, char *end) {
	cherokee_buffer_add_va (buf, "%d %s %s", zoom, start, end);
}

static void
parse_changes(cherokee_avl_t *arguments, cherokee_buffer_t *buf) {
	unsigned long int hours;
	unsigned long int zoom;
	void *param;
	ret_t                 ret;

	ret = cherokee_avl_get_ptr (arguments, "zoom", &param);
	if (ret == ret_ok) {
		zoom = strtoul(param, (char **) NULL, 10);
	} else
		zoom = 12;

	ret = cherokee_avl_get_ptr (arguments, "start", &param);
	if (ret == ret_ok) {
		void *param2;
		ret = cherokee_avl_get_ptr (arguments, "end", &param2);
		if (ret == ret_ok) {
			changes_bystartend(buf, zoom, (char *)param, (char *)param2);
		}
	} else { 
		ret = cherokee_avl_get_ptr (arguments, "hours", &param);
		if (ret == ret_ok) {
			hours = strtoul(param, (char **) NULL, 10);
		} else
			hours = 1;

		changes_byhour(buf, zoom, hours);
	}
}

	static void
objtype_history(cherokee_buffer_t *buf, unsigned long int id, osm_state_get_t state)
{
	printf("%s\n", __func__);
}


	static ret_t
osm_build_page (cherokee_handler_osm_t *hdl)
{
	ret_t                 ret = ret_ok;
	cherokee_server_t     *srv;
	cherokee_connection_t *conn;
	cherokee_buffer_t     *buf;
	cherokee_buffer_t     content = CHEROKEE_BUF_INIT;

	/* Init
	*/
	buf = &hdl->buffer;
	srv = HANDLER_SRV(hdl);
	conn = HANDLER_CONN(hdl);

	/* Parse the Request */

	if (conn->request.len > 1) {
		osm_state_get_t state = OSM_FIND_FIRST;
		unsigned long int id = 0;
		char *string = strdupa(conn->request.buf);
		char *token;
		char *xapi_offset;
		// void *param;
		while (state != OSM_DONE && (token = (char *) strsep( &string , "/")) != NULL) {
			if (*token == '\0')
				continue;

			switch (state) {
				case OSM_FIND_FIRST:
					switch (token[0]) {
						case '*':
							if (token[1] == '[') {
								xapi_offset = &token[1];
								state = OSM_ALL_XAPI_PARSE;
							} else
								state = OSM_DONE;
							break;
						case 'c':
							/* GET /api/0.5/changes?hours=1&zoom=16&start=none&end=none */
							parse_changes(conn->arguments, &content);
							state = OSM_DONE;
							break;
						case 'n':
							if (strncmp(token, "node", 4) == 0) {
								switch (token[4]) {
									case '\0':
										state = OSM_NODE_ID;
										break;
									case '[':
										xapi_offset = &token[4];
										state = OSM_NODE_XAPI_PARSE;
										break;
									case 's':
										state = OSM_NODES_PRE_PARSE;
										break;
								}
							} else {
								state = OSM_DONE;
							}
							break;
						case 'w':
							if (strncmp(token, "way", 3) == 0) {
								switch (token[3]) {
									case '\0':
										state = OSM_WAY_ID;
										break;
									case '[':
										xapi_offset = &token[3];
										state = OSM_WAY_XAPI_PARSE;
										break;
									case 's':
										state = OSM_WAYS_PRE_PARSE;
										break;
								}
							} else 
								state = OSM_DONE;
							
							break;
						case 'r':
							if (strncmp(token, "relation", 8) == 0) {
								switch (token[8]) {
									case '\0':
										state = OSM_RELATION_ID;
										break;
									case '[':
										xapi_offset = &token[8];
										state = OSM_RELATION_XAPI_PARSE;
										break;
									case 's':
										state = OSM_RELATIONS_PRE_PARSE;
										break;
								}
							} else 
								state = OSM_DONE;
							
							break;
						case 'g':
							state = OSM_GPX_ID;
							break;
						case 'u':
							user_preferences(&content);
							break;
						case 'm':
							/* GET /api/0.5/map?bbox=LEFT,BOTTOM,RIGHT,TOP */
							parse_map2(hdl, &content);
							break;
						case 't':
							/* GET /api/0.5/trackpoints?bbox=LEFT,BOTTOM,RIGHT,TOP&page=PAGENUMBER */
							parse_trackpoints(conn->arguments, &content);
							break;
						case 's':
							break;
					}
					break;


				case OSM_GPX_ID:
				case OSM_NODE_ID:
				case OSM_WAY_ID:
				case OSM_RELATION_ID:
					id = strtoul(token, (char **) NULL, 10);
					if (errno != ERANGE) {
						state++;
					} else 
						state = OSM_DONE;
					break;
				case OSM_GPX_COMMAND:
					if (strlen(token) > 1) {
						switch (token[1]) {
							case 'a':
								/* GET /api/0.5/gpx/<id>/data */
								gpx_data(&content, id);
								break;
							case 'e':
								/* GET /api/0.5/gpx/<id>/details */
								gpx_details(&content, id);
								break;
						}
					}
					break;

					/* GET /api/0.5/<objtype>/<id>/full
					 * GET /api/0.5/<objtype>/<id>/history
					 * GET /api/0.5/<objtype>/<id>/relations */

				case OSM_NODE_COMMAND:
					switch (token[0]) {
						case 'w':
							/* GET /api/0.5/node/<id>/ways */
							state++;
							break;
						case 'r':
							/* GET /api/0.5/?/<id>/relations */
							state += 2;
							break;
					}
					break;
				case OSM_WAY_COMMAND:
				case OSM_RELATION_COMMAND:
					switch (token[0]) {
						case 'f':
							/* GET /api/0.5/?/<id>/full */
							state++;
							break;
						case 'h':
							/* GET /api/0.5/?/<id>/history */
							objtype_history(&content, id, state);
							break;
						case 'r':
							/* GET /api/0.5/?/<id>/relations */
							state += 2;
							break;
					}
					break;
				case OSM_NODES_PRE_PARSE:
					switch (token[0]) {
						case '?':
							/* GET /api/0.5/<objtype>s?<objtype>s=<id>[,<id>...] */
							parse_nodes(conn->arguments, &content);
							break;
						case 's':
							/* GET /api/0.5/nodes/search?type=<type>&value=<value> */
							parse_nodes_search(conn->arguments, &content);
							break;
					}
					break;

				case OSM_RELATIONS_PRE_PARSE:
					switch (token[0]) {
						case '?':
							/* GET /api/0.5/<objtype>s?<objtype>s=<id>[,<id>...] */
							parse_relations(conn->arguments, &content);
							break;
						case 's':
							/* GET /api/0.5/relations/search?type=<type>&value=<value> */
							parse_relations_search(conn->arguments, &content);
							break;
					}
					break;

				case OSM_WAYS_PRE_PARSE:
					switch (token[0]) {
						case '?':
							/* GET /api/0.5/<objtype>s?<objtype>s=<id>[,<id>...] */
							parse_ways(conn->arguments, &content);
							break;
						case 's':
							/* GET /api/0.5/ways/search?type=<type>&value=<value> */
							parse_ways_search(conn->arguments, &content);
							break;
					}
					break;

				default:
					state = OSM_DONE;
			}
		}

		switch (state) {
			case OSM_GPX_ID:
			case OSM_NODE_ID:
			case OSM_WAY_ID:
			case OSM_RELATION_ID:
			case OSM_NODE_COMMAND:
			case OSM_WAY_COMMAND:
			case OSM_RELATION_COMMAND:
			case OSM_NODE_COMMAND_WAYS:
			case OSM_WAY_COMMAND_FULL:
			case OSM_RELATION_COMMAND_FULL:
			case OSM_NODE_COMMAND_RELATIONS:
			case OSM_WAY_COMMAND_RELATIONS:
			case OSM_RELATION_COMMAND_RELATIONS:
				ret = get_object_by_id(hdl, id, &content, state);
			default:
				break;
		}

		if (state == OSM_NODE_XAPI_PARSE ||
				state == OSM_WAY_XAPI_PARSE ||
				state == OSM_RELATION_XAPI_PARSE ||
				state == OSM_ALL_XAPI_PARSE) {
			char *key, *value, *final;
			unsigned short int first_tag = TRUE;
			unsigned short int first_bbox = TRUE;
			cherokee_buffer_t sql = CHEROKEE_BUF_INIT; 
			cherokee_buffer_t sql_tags = CHEROKEE_BUF_INIT; 
			cherokee_buffer_t sql_bbox = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;

			key = xapi_offset;

			while (*key == '[' && (value = ++key) != NULL && 
					(key = strsep(&value, "=")) != NULL && (final = value) != NULL &&
					(value = strsep(&final, "]")) != NULL
			      ) { 
				TRACE("xapi", "%s %s\n", key, value);

				if (strcmp(key, "bbox") == 0) {
					double left, bottom, right, top;
					if (fetch_bbox(value, &left, &bottom, &right, &top) == ret_ok) {
						if (first_bbox == FALSE) {
							cherokee_buffer_add_str (&sql_bbox, " OR ");
						} else
							first_tag = FALSE;
						cherokee_buffer_add_str (&sql_bbox, "(");	
						cherokee_buffer_add_va(&sql_bbox, SQL_BY_BBOX, BBOX_VA_ARGS);
						cherokee_buffer_add_str (&sql_bbox, ")");
					}
				} else {
					if (first_tag == FALSE) {
						cherokee_buffer_add_str (&sql_tags, " OR ");
					} else
						first_tag = FALSE;

					cherokee_buffer_t temp2 = CHEROKEE_BUF_INIT;
					cherokee_buffer_add(&temp2, value, strlen(value));
					cherokee_buffer_replace_string (&temp2, "'", 1, "\\\'", 2); /* escape quotes */
					cherokee_buffer_replace_string (&temp2, "|", 1, "', '", 4);

					if (strcmp(key, "*") == 0) {
						if (strcmp(value, "*") != 0) {
							cherokee_buffer_add_va (&sql_tags, "(v IN ('%s'))", temp2.buf);
						}
					} else {
						cherokee_buffer_t temp1 = CHEROKEE_BUF_INIT;
						cherokee_buffer_add(&temp1, key, strlen(key));
						cherokee_buffer_replace_string (&temp1, "'", 1, "\\\'", 2); /* escape quotes */
						cherokee_buffer_replace_string (&temp1, "|", 1, "', '", 4);
						if (strcmp(value, "*") == 0) {
							cherokee_buffer_add_va (&sql_tags, "(k IN ('%s'))", temp1.buf);
						} else {
							cherokee_buffer_add_va (&sql_tags, "(k IN ('%s') AND v IN ('%s'))", temp1.buf, temp2.buf);
						}
						cherokee_buffer_mrproper(&temp1);
					}

					cherokee_buffer_mrproper(&temp2);
				}
				key = final;
			}

			if (sql_tags.len > 0) {
				cherokee_buffer_add_str (&sql, " AND (");
				cherokee_buffer_add_buffer (&sql, &sql_tags);
				cherokee_buffer_add_str (&sql, ")");
			}

			if (sql_bbox.len > 0) {
				cherokee_buffer_add_str (&sql, " AND (");
				cherokee_buffer_add_buffer (&sql, &sql_bbox);
				cherokee_buffer_add_str (&sql, ")");
			}


			if (state == OSM_NODE_XAPI_PARSE || state == OSM_ALL_XAPI_PARSE) {

				cherokee_buffer_add_str (&sql1, SQL_XAPI_NODE);
				cherokee_buffer_add_buffer (&sql1, &sql);
				cherokee_buffer_add_str (&sql1, SQL_XAPI_NODE_END);
				TRACE("xapi", "%s\n", sql1.buf);

			}
			
			cherokee_buffer_t sql_id = CHEROKEE_BUF_INIT;
			cherokee_buffer_add_str (&sql_id, SQL_XAPI_NODE_ID);
			cherokee_buffer_add_buffer (&sql_id, &sql);

			if (state == OSM_NODE_XAPI_PARSE || state == OSM_ALL_XAPI_PARSE) {

				cherokee_buffer_add_str (&sql2, SQL_XAPI_NTAG);
				cherokee_buffer_add_buffer (&sql2, &sql_id);
				cherokee_buffer_add_str (&sql2, SQL_XAPI_NTAG_END);
				TRACE("xapi", "%s\n", sql2.buf);


				run_sql2(hdl, &sql1, &sql2, &content, result_nodenew_to_xml);

				cherokee_buffer_mrproper(&sql1);
				cherokee_buffer_mrproper(&sql2);
			}

			if (state == OSM_WAY_XAPI_PARSE || state == OSM_ALL_XAPI_PARSE) {
				cherokee_buffer_t sql_way_id = CHEROKEE_BUF_INIT;
				cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;

				cherokee_buffer_add_str (&sql1, SQL_XAPI_WAY);
				cherokee_buffer_add_str (&sql1, " AND (");
				cherokee_buffer_add_buffer (&sql1, &sql_tags);
				cherokee_buffer_add_str (&sql1, ")");
				cherokee_buffer_add_str (&sql1, SQL_XAPI_WAY_MID);
				cherokee_buffer_add_buffer (&sql1, &sql_id);
				cherokee_buffer_add_str (&sql1, SQL_XAPI_WAY_END);
				TRACE("xapi", "%s\n", sql1.buf);

				cherokee_buffer_add_str (&sql_way_id, SQL_XAPI_WAY_ID);
				cherokee_buffer_add_str (&sql_way_id, " AND (");
				cherokee_buffer_add_buffer (&sql_way_id, &sql_tags);
				cherokee_buffer_add_str (&sql_way_id, ")");
				cherokee_buffer_add_str (&sql_way_id, SQL_XAPI_WAY_MID);
				cherokee_buffer_add_buffer (&sql_way_id, &sql_id);
				cherokee_buffer_add_str (&sql_way_id, SQL_XAPI_WAY_ID_END);
			
				cherokee_buffer_add_str (&sql2, SQL_XAPI_WAY_WT);
				cherokee_buffer_add_buffer (&sql2, &sql_way_id);
				cherokee_buffer_add_str (&sql2, SQL_XAPI_WAY_WT_END);
				TRACE("xapi", "%s\n", sql2.buf);
			
				cherokee_buffer_add_str (&sql3, SQL_XAPI_WAY_WN);
				cherokee_buffer_add_buffer (&sql3, &sql_way_id);
				cherokee_buffer_add_str (&sql3, SQL_XAPI_WAY_WN_END);
				TRACE("xapi", "%s\n", sql3.buf);
			
				run_sql3(hdl, &sql1, &sql2, &sql3, &content, result_way_to_xml);

				// TRACE("xapi", "%s\n", content.buf);

				cherokee_buffer_mrproper(&sql1);
				cherokee_buffer_mrproper(&sql2);
				cherokee_buffer_mrproper(&sql3);

				cherokee_buffer_mrproper(&sql_way_id);
			}

			if (state == OSM_RELATION_XAPI_PARSE || state == OSM_ALL_XAPI_PARSE) {
				cherokee_buffer_t sql_rel_id = CHEROKEE_BUF_INIT;
				cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;
				cherokee_buffer_t sql4 = CHEROKEE_BUF_INIT;
				cherokee_buffer_t sql5 = CHEROKEE_BUF_INIT;

				cherokee_buffer_add_str (&sql1, SQL_XAPI_REL);
				cherokee_buffer_add_str (&sql1, " AND (");
				cherokee_buffer_add_buffer (&sql1, &sql_tags);
				cherokee_buffer_add_str (&sql1, ")");
				cherokee_buffer_add_str (&sql1, SQL_XAPI_REL_MID);
				cherokee_buffer_add_buffer (&sql1, &sql_id);
				cherokee_buffer_add_str (&sql1, SQL_XAPI_REL_END);
				TRACE("xapi", "%s\n", sql1.buf);

				cherokee_buffer_add_str (&sql_rel_id, SQL_XAPI_REL_ID);
				cherokee_buffer_add_str (&sql_rel_id, " AND (");
				cherokee_buffer_add_buffer (&sql_rel_id, &sql_tags);
				cherokee_buffer_add_str (&sql_rel_id, ")");
				cherokee_buffer_add_str (&sql_rel_id, SQL_XAPI_REL_MID);
				cherokee_buffer_add_buffer (&sql_rel_id, &sql_id);
				cherokee_buffer_add_str (&sql_rel_id, SQL_XAPI_REL_ID_END);
			
				cherokee_buffer_add_str (&sql2, SQL_XAPI_REL_RT);
				cherokee_buffer_add_buffer (&sql2, &sql_rel_id);
				cherokee_buffer_add_str (&sql2, SQL_XAPI_REL_RT_END);
				TRACE("xapi", "%s\n", sql2.buf);
			
				cherokee_buffer_add_str (&sql3, SQL_XAPI_REL_RN);
				cherokee_buffer_add_buffer (&sql3, &sql_rel_id);
				cherokee_buffer_add_str (&sql3, SQL_XAPI_REL_RN_END);
				TRACE("xapi", "%s\n", sql3.buf);
			
				cherokee_buffer_add_str (&sql4, SQL_XAPI_REL_RR);
				cherokee_buffer_add_buffer (&sql4, &sql_rel_id);
				cherokee_buffer_add_str (&sql4, SQL_XAPI_REL_RR_END);
				TRACE("xapi", "%s\n", sql4.buf);
				
				cherokee_buffer_add_str (&sql5, SQL_XAPI_REL_RW);
				cherokee_buffer_add_buffer (&sql5, &sql_rel_id);
				cherokee_buffer_add_str (&sql5, SQL_XAPI_REL_RW_END);
				TRACE("xapi", "%s\n", sql4.buf);
		
				run_sql5(hdl, &sql1, &sql2, &sql3, &sql4, &sql5, &content, result_relation_to_xml);

				cherokee_buffer_mrproper(&sql1);
				cherokee_buffer_mrproper(&sql2);
				cherokee_buffer_mrproper(&sql3);
				cherokee_buffer_mrproper(&sql4);
				cherokee_buffer_mrproper(&sql5);

				cherokee_buffer_mrproper(&sql_rel_id);
			}

			cherokee_buffer_mrproper(&sql);
			cherokee_buffer_mrproper(&sql_id);
			cherokee_buffer_mrproper(&sql_tags);
			cherokee_buffer_mrproper(&sql_bbox);
		}
	}

	
	cherokee_buffer_add_str (buf, XMLHEADER);
	cherokee_buffer_add_str (buf, XML(OSM, ""));
	if (content.len > 0) {
		ret_t ret;
		cherokee_buffer_add_str (buf, XMLCONTINUE);
		ret = cherokee_buffer_add_buffer (buf, &content);
		cherokee_buffer_add_str (buf, XMLCLOSE(OSM, ""));
	} else 
		cherokee_buffer_add_str (buf, XMLCLOSESHORT);

	cherokee_buffer_mrproper (&content);

	return ret_ok;
}

ret_t
cherokee_handler_osm_init_get (cherokee_handler_osm_t *hdl) {
	ret_t ret;
	cherokee_connection_t *conn = HANDLER_CONN(hdl);

	ret = cherokee_connection_parse_args (conn);
	if (unlikely(ret != ret_ok)) {
		conn->error_code = http_internal_error;
		return ret_error;
	}

	/* integrate this later */
	ret = osm_build_page(hdl);

	return ret;
}


