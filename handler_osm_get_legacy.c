#if BYRESULT
static ret_t result_id_to_list(cherokee_handler_osm_t *hdl, MapiHdl *anything, cherokee_buffer_t *buf) {
	if (mapi_get_row_count(*anything) != 0) {
		while (mapi_fetch_row(*anything)) {
			char *field = mapi_fetch_field(*anything, 0);
			if (field != NULL) {
				cherokee_buffer_add_va (buf, "%s, ", field);
			}
		}
	}

	cherokee_buffer_drop_ending(buf, 2);
	return ret_ok;
}
#endif


//#define BYRESULT 1

#ifdef BYRESULT
static void parse_map2(cherokee_handler_osm_t *hdl, cherokee_buffer_t *buf) {
	double left, bottom, right, top;
	cherokee_avl_t *arguments = HANDLER_CONN(hdl)->arguments;

	if (ret_ok == fetch_bbox(arguments, &left, &bottom, &right, &top)) {
		TRACE ("osmapi", "%s BBOX: %f %f %f %f\n", __func__, left, bottom, right, top);
		cherokee_buffer_t sql1 = CHEROKEE_BUF_INIT;
		cherokee_buffer_t nodes = CHEROKEE_BUF_INIT;
		cherokee_buffer_t relations = CHEROKEE_BUF_INIT;

		cherokee_buffer_add_va (&sql1, "SELECT id FROM " SQL_NODES " WHERE " SQL_BY_BBOX, BBOX_VA_ARGS);
		run_sql(hdl, &sql1, &nodes, result_id_to_list);
		cherokee_buffer_mrproper(&sql1);

		if (nodes.len > 0) {
			cherokee_buffer_t sql2 = CHEROKEE_BUF_INIT;

			cherokee_buffer_add_str (&sql1, "SELECT relation FROM members_node WHERE to_node IN (");
			cherokee_buffer_add_buffer(&sql1, &nodes);
			cherokee_buffer_add_str (&sql1, ")");
			run_sql(hdl, &sql1, &relations, result_id_to_list);
			cherokee_buffer_mrproper(&sql1);


			if (relations.len > 0) {
				cherokee_buffer_add_str (&nodes, ", ");
				cherokee_buffer_add_str (&sql1, "SELECT to_node FROM members_node WHERE relation IN (");
				cherokee_buffer_add_buffer(&sql1, &relations);
				cherokee_buffer_add_str (&sql1, ")");
				run_sql(hdl, &sql1, &nodes, result_id_to_list);
				cherokee_buffer_mrproper(&sql1);
			}

			cherokee_buffer_add_str (&sql1, "SELECT id, long, lat, username, timestamp FROM " SQL_NODES " WHERE id IN (");
			cherokee_buffer_add_buffer(&sql1, &nodes);
			cherokee_buffer_add_str (&sql1, ") ORDER BY id");
		
			cherokee_buffer_add_str (&sql2, "SELECT node, k, v FROM node_tags WHERE node IN (");
			cherokee_buffer_add_buffer(&sql2, &nodes);
			cherokee_buffer_add_str (&sql2, ") ORDER BY node");

			run_sql2(hdl, &sql1, &sql2, buf, result_nodenew_to_xml);

			cherokee_buffer_mrproper(&sql1);
			cherokee_buffer_mrproper(&sql2);
	
			cherokee_buffer_mrproper(&nodes);

			if (relations.len > 0) {
				cherokee_buffer_t sql3 = CHEROKEE_BUF_INIT;
				cherokee_buffer_t sql4 = CHEROKEE_BUF_INIT;

				cherokee_buffer_add_str (&sql1, "SELECT id, username, timestamp FROM relations WHERE id IN (");
				cherokee_buffer_add_buffer(&sql1, &relations);
				cherokee_buffer_add_str (&sql1, ") ORDER BY id");
				cherokee_buffer_add_str (&sql2, "SELECT relation, k, v FROM relation_tags WHERE relation IN (");
				cherokee_buffer_add_buffer(&sql2, &relations);
				cherokee_buffer_add_str (&sql2, ") ORDER BY relation");
				cherokee_buffer_add_str (&sql3, "SELECT relation, to_relation, role FROM members_relation WHERE relation IN (");
				cherokee_buffer_add_buffer(&sql3, &relations);
				cherokee_buffer_add_str (&sql3, ") ORDER BY relation, idx");
				cherokee_buffer_add_str (&sql4, "SELECT relation, to_node, role FROM members_node WHERE relation IN (");
				cherokee_buffer_add_buffer(&sql4, &relations);
				cherokee_buffer_add_str (&sql4, ") ORDER BY relation, idx");

				run_sql4(hdl, &sql1, &sql2, &sql3, &sql4, buf, result_relationway_to_xml);

				cherokee_buffer_mrproper(&sql1);
				cherokee_buffer_mrproper(&sql2);
				cherokee_buffer_mrproper(&sql3);
				cherokee_buffer_mrproper(&sql4);
			}

			cherokee_buffer_mrproper(&relations);
		}
	}
}
#else 
#endif


