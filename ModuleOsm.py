from Form import *
from Table import *
from ModuleHandler import *
from validations import *
from consts import *


NOTE_READ_ONLY	    = 'Never write to the database.'
NOTE_DB_HOSTNAME    = 'The hostname of your database server.'
NOTE_DB_PORT	    = 'The port your database server listens on.'
NOTE_DB_USERNAME    = 'The username you want to connect to your database.'
NOTE_DB_PASSWORD    = 'The password to connect to your database (warning security risk).'
NOTE_DB_NAME	    = 'The database you want to connect to.'

HELPS = [
    ('modules_handlers_osm', "OSM")
]

class ModuleOsm (ModuleHandler):
    PROPERTIES = [
        'read_only',
	'db_hostname',
	'db_port',
	'db_username',
	'db_password',
	'db_name'
    ]

    def __init__ (self, cfg, prefix, submit):
        ModuleHandler.__init__ (self, 'osm', cfg, prefix, submit)
	self.show_document_root = False

    def _op_render (self):
    	txt   = ''

        txt += "<h2>Osm options</h2>"

        table = TableProps()
	self.AddPropEntry (table, "DB Hostname", "%s!db_hostname" % (self._prefix),   NOTE_DB_HOSTNAME)
	self.AddPropEntry (table, "DB Port", "%s!db_port" % (self._prefix), 	      NOTE_DB_PORT)
	self.AddPropEntry (table, "DB Username", "%s!db_username" % (self._prefix),   NOTE_DB_USERNAME)
	self.AddPropEntry (table, "DB Password", "%s!db_password" % (self._prefix),   NOTE_DB_PASSWORD)
	self.AddPropEntry (table, "DB Name", "%s!db_name" % (self._prefix),   	      NOTE_DB_NAME)
        self.AddPropCheck (table, "Read Only", "%s!read_only" % (self._prefix), True, NOTE_READ_ONLY)
        txt += self.Indent(table)

        return txt

    def _op_apply_changes (self, uri, post):
	checkboxes = ['read_only']
	self.ApplyChangesPrefix (self._prefix, checkboxes, post)
