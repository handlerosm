#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>

typedef enum {
	OSM_FIND_FIRST = 0,
	OSM_GPX_ID,
	OSM_GPX_COMMAND,
	OSM_NODE_ID,
	OSM_NODE_COMMAND,
	OSM_NODE_COMMAND_WAYS,
	OSM_NODE_COMMAND_RELATIONS,
	OSM_NODES,
	OSM_NODES_PRE_PARSE,
	OSM_WAY_ID,
	OSM_WAY_COMMAND,
	OSM_WAY_COMMAND_FULL,
	OSM_WAY_COMMAND_RELATIONS,
	OSM_WAYS_PRE_PARSE,
	OSM_WAYS_SEARCH,
	OSM_RELATION_ID,
	OSM_RELATION_COMMAND,
	OSM_RELATION_COMMAND_FULL,
	OSM_RELATION_COMMAND_RELATIONS,
	OSM_RELATIONS_PRE_PARSE,
	OSM_RELATIONS_SEARCH,
	OSM_NODE_XAPI_PARSE,
	OSM_WAY_XAPI_PARSE,
	OSM_RELATION_XAPI_PARSE,
	OSM_ALL_XAPI_PARSE,
	OSM_DONE
} osm_state_get_t;

ret_t get_object_by_id(cherokee_handler_osm_t *hdl, unsigned long int id, cherokee_buffer_t *buf, osm_state_get_t state);
ret_t cherokee_handler_osm_init_get (cherokee_handler_osm_t *hdl);
