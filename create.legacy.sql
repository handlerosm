CREATE TABLE usernames (id serial, username varchar(255), password varchar(255));
INSERT INTO usernames (id, username) VALUES (0, 'unknown');
CREATE TABLE nodes_legacy (id serial, x double, y double, username integer, timestamp timestamptz, foreign key(username) references usernames, visible boolean default true);
CREATE TABLE node_tags (node integer, k varchar(255), v varchar(1024), primary key (node, k), foreign key(node) references nodes_legacy);
CREATE TABLE relations(id serial, username integer, timestamp timestamptz, foreign key(username) references usernames, visible boolean default true);
CREATE TABLE members_node (relation integer, idx integer, to_node integer, role varchar(255), foreign key(relation) references relations, foreign key(to_node) references nodes_legacy, primary key(relation, idx));
CREATE TABLE members_relation (relation integer, idx integer, to_relation integer, role varchar(255), foreign key(relation) references relations, foreign key(to_relation) references relations, primary key(relation, idx));
CREATE TABLE relation_tags (relation integer, k varchar(255), v varchar(1024), foreign key(relation) references relations, primary key(relation, k)); 
