#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>

/*
 * <osm>
 *  <node>
 *   <tag k=".." v=".." />
 *  </node>
 *  <way>
 *   <nd>
 *   <tag>
 *  </way>
 *  <relation>
 *   <member>
 *   <tag>
 *  </relation>
 */

/* TODO:
 * Do something usefull with changesets and version;
 */

#ifdef MMAP
# define stopcondition (start = ++end) < (range + max)
# define nextline start = end + 1
#else
# define nextline free(start); start = NULL; tmp = getline(&start, &tmplen, stdin); if (tmp == -1) { goto exit; }
# define stopcondition 1
#endif

#define file_delete_nodes               "delete_nodes.csv"
#define file_delete_ways                "delete_ways.csv"
#define file_delete_relations           "delete_relations.csv"

#define file_modify_nodes               "modify_nodes.csv"
#define file_modify_ways                "modify_ways.csv"
#define file_modify_relations           "modify_relations.csv"

#define file_nodes			"nodes.csv"

#ifdef BENCHMARK
#define file_nodes_uint			"nodes_uint.csv"
#define file_nodes_gis			"nodes_gis.csv"
#endif

#define file_node_tags			"node_tags.csv"
#define file_ways			"ways.csv"
#define file_way_tags			"way_tags.csv"
#define file_way_nds			"way_nds.csv"
#define file_relations			"relations.csv"
#define file_relation_tags		"relation_tags.csv"
#define file_relation_member_node	"relation_member_node.csv"
#define file_relation_member_relation	"relation_member_relation.csv"
#define file_relation_member_way	"relation_member_way.csv"

/* http://www.cs.utk.edu/~vose/c-stuff/bithacks.html */
static unsigned int reverse(unsigned int v) {
        unsigned int t = v << 1;  // t will have the reversed bits of v
        int i;

        v >>= 1;
        for (i = sizeof(v) * 8 - 2; i; i--)
        {
          t |= v & 1;
          t <<= 1;
          v >>= 1;
        }
        t |= v;

        return t;
}

static unsigned long zcurve(char *lon, char *lat) {
	unsigned int array[2];
	unsigned int i,j;
	unsigned long out;
	double temp = strtod(lon, (char **) NULL) + 180.0; /* Longitude */
	if (temp < 0.0 || temp >= 360.0) temp = 0.0;
	array[0] = reverse(  5965232.0 * temp);
	
	temp = strtod(lat, (char **) NULL) + 90.0; /* Latitude */
	if (temp < 0.0 || temp >= 180.0) temp = 0.0;
	array[1] = reverse( 11930464.0 * temp);

	for (j = 0; j < sizeof(unsigned int)*8; j++) {
		for (i = 0; i < 2; i++) {
			out <<= 1;
			out |= array[i] & 1;
			array[i] >>= 1;
		}
	}

	return out;
}


static unsigned int coordtouint(char *input) {
	double maxbit = (double) 4294967296.0 / (double) 360.0;
	double proper = strtod(input, NULL) * maxbit;
	return (unsigned int) proper;
}

static char * escape_string(char *instr)
{
	unsigned int i, j=0, need = 0;
	unsigned int len = strlen(instr);
	char *outstr;
	
	for (i=0;i<len;i++)
		if (instr[i]=='\\' || instr[i]=='\'') need++;

	len += need;
	outstr = malloc(len + 1);
 
   	for (i=0;i<=strlen(instr);i++) {
	      if (instr[i]=='\\' || instr[i]=='\'') 
	       outstr[j++]='\\';
	      outstr[j++]=instr[i];
    }
    return outstr;
}

#ifdef MMAP
static void parser(char *range, unsigned long int max) {
#else
static void parser() {
#endif
    typedef enum { NONE = 0, CREATE = 1, MODIFY = 2, DELETE = 3 } ywk_state_t;
    typedef enum { OSM = 0, NODE = 1, WAY = 2, RELATION = 3, TAG = 4, ND = 5, MEMBER = 6 } osm_state_t;
    typedef enum { UNKNOWN = 0, ID, LAT, LON, USER, UID, TIMESTAMP, KEY, VALUE, TYPE, REF, ROLE, VERSION, CHANGESET} key_state_t;
    char *attr_id = NULL, *attr_lat = NULL, *attr_lon = NULL, *attr_user = NULL, *attr_uid = NULL, *attr_timestamp = NULL, *attr_changeset = NULL, *attr_key = NULL, *attr_value = NULL,
         *attr_type = NULL, *attr_ref = NULL, *attr_role = NULL, *attr_version = NULL;

    unsigned int attr_lat_uint = 0;
    unsigned int attr_lon_uint = 0;


    unsigned long int count_nodes = 0, count_node_tags = 0,
    count_ways = 0, count_way_tags = 0, count_way_nds = 0,
    count_relations = 0, count_relation_tags = 0, count_members_node = 0, count_members_relation = 0, count_members_way = 0,
    count_delete_nodes = 0, count_delete_ways = 0, count_delete_relations = 0,
    count_modify_nodes = 0, count_modify_ways = 0, count_modify_relations = 0;

    unsigned long int sequence = 0;

    ywk_state_t ywk = NONE;
    osm_state_t current_tag = OSM;
    osm_state_t parent_tag = OSM;

    char *start = NULL, *end, *nodename, *nodename_end, *nodename_fast_end;
    ssize_t tmp;
    size_t tmplen = 0;

#ifdef MMAP
    start = range;
#else
    nextline;
#endif
    end = strchrnul((const char*) start, '\n');

    if (strncmp(start, "<?xml", 5) != 0)
        return;

    nextline;
    end = strchrnul((const char*) start, '\n');

    if (strncmp(start, "<osm", 4) != 0)
        return;

    if (strncmp(&start[4], "Change", 6) == 0)
	ywk = CREATE;

FILE *fd_nodes,
#ifdef BENCHMARK
    *fd_nodes_uint, 
    *fd_nodes_gis,
#endif
    *fd_node_tags,
    *fd_ways,
    *fd_way_tags,
    *fd_way_nds,
    *fd_relations,
    *fd_relation_tags,
    *fd_members_node,
    *fd_members_relation,
    *fd_members_way,
    *fd_delete_nodes,
    *fd_delete_ways,
    *fd_delete_relations,
    *fd_modify_nodes,
    *fd_modify_ways,
    *fd_modify_relations
    ;

    fd_nodes = fopen(file_nodes, "w");
    if (fd_nodes == NULL) { perror("Open:"); exit(-1); }
#ifdef BENCHMARK
    fd_nodes_uint = fopen(file_nodes_uint, "w");
    if (fd_nodes_uint == NULL) { perror("Open:"); exit(-1); }
    fd_nodes_gis = fopen(file_nodes_gis, "w");
    if (fd_nodes_gis == NULL) { perror("Open:"); exit(-1); }
#endif
    fd_node_tags = fopen(file_node_tags, "w");
    if (fd_node_tags == NULL) { perror("Open:"); exit(-1); }
   fd_ways = fopen(file_ways, "w");
   if (fd_ways == NULL) { perror("Open:"); exit(-1); }
   	fd_way_tags = fopen(file_way_tags, "w");
    	if (fd_way_tags == NULL) { perror("Open:"); exit(-1); }
    	fd_way_nds = fopen(file_way_nds, "w"); 
    	if (fd_way_nds == NULL) { perror("Open:"); exit(-1); }
    	fd_relations = fopen(file_relations, "w");
    	if (fd_relations == NULL) { perror("Open:"); exit(-1); }
    	fd_relation_tags = fopen(file_relation_tags, "w");
    	if (fd_relation_tags == NULL) { perror("Open:"); exit(-1); }
    	fd_members_node = fopen(file_relation_member_node, "w");
    	if (fd_members_node == NULL) { perror("Open:"); exit(-1); }
    	fd_members_relation = fopen(file_relation_member_relation, "w");
    	if (fd_members_relation == NULL) { perror("Open:"); exit(-1); }
    	fd_members_way = fopen(file_relation_member_way, "w");
    	if (fd_members_way == NULL) { perror("Open:"); exit(-1); }


      	fd_delete_nodes = fopen(file_delete_nodes, "w");
    	if (fd_delete_nodes == NULL) { perror("Open:"); exit(-1); }
      	fd_delete_ways = fopen(file_delete_ways, "w");
    	if (fd_delete_ways == NULL) { perror("Open:"); exit(-1); }
      	fd_delete_relations = fopen(file_delete_relations, "w");
    	if (fd_delete_relations == NULL) { perror("Open:"); exit(-1); }
      	
        fd_modify_nodes = fopen(file_modify_nodes, "w");
    	if (fd_modify_nodes == NULL) { perror("Open:"); exit(-1); }
      	fd_modify_ways = fopen(file_modify_ways, "w");
    	if (fd_modify_ways == NULL) { perror("Open:"); exit(-1); }
      	fd_modify_relations = fopen(file_modify_relations, "w");
    	if (fd_modify_relations == NULL) { perror("Open:"); exit(-1); }


    nextline;

    do {
    end = strchrnul((const char*) start, '\n');

    nodename = strchrnul(start, '<') + 1;
    nodename_fast_end = strchrnul(nodename, '>');
    nodename_end = strchrnul(nodename, ' ');

    if (nodename_fast_end < nodename_end) nodename_end = nodename_fast_end;

    if (nodename[0] == '/') {
        free(attr_id);
        free(attr_lat);
        free(attr_lon);
        free(attr_timestamp);
        free(attr_changeset);
        free(attr_user);
        free(attr_uid);
        free(attr_version);
        
        attr_id = attr_lat = attr_lon = attr_user = attr_uid = attr_timestamp = attr_changeset = attr_version = NULL;

	sequence = 0;

	nextline;
        continue;
    }
    
    switch (nodename_end - nodename) {
        case 2:
            current_tag = ND;
            break;
        case 3: {
                    switch (nodename[0]) {
                        case 'o':
                            current_tag = OSM;  
                            break;
                        case 'w':
                            current_tag = WAY;
                            break;
                        case 't':
                            current_tag = TAG;
                            break;
                        default:
                            fprintf(stderr, "--> %c%c", nodename[0], nodename[1]);
                    }
                    break;
                }
        case 4:
                current_tag = NODE;
                break;
	case 5:
		/* BOUND */
		nextline;
		continue;
        case 6:
		switch (nodename[2]) {
			case 'e':
				ywk = CREATE;
				nextline;
				continue;
				break;
			case 'd':
				ywk = MODIFY;
				nextline;
				continue;
				break;
			case 'l':
				ywk = DELETE;
				nextline;
				continue;
				break;
			case 'm':
				current_tag = MEMBER;
				break;
			default:
				fprintf(stderr, "--> %c%c", nodename[0], nodename[1]);
		}

                break;
        case 8:
                current_tag = RELATION;
                break;
        default:
                fprintf(stderr, "--> %c%c", nodename[0], nodename[1]);
    }


    char *key, *key_end, *value_end;
    key = nodename_end + 1;

    do {
        char *value;
        key_state_t current_key = UNKNOWN;
        key_end = strchrnul(key, '=');

        if (key_end == NULL || key_end >= end)
		break;

        switch (key_end - key) {
            case 1: {
                    switch (key[0]) {
                        case 'k':
                            current_key = KEY;
                            break;
                        case 'v':
                            current_key = VALUE;
                            break;
                        default:
                            current_key = UNKNOWN;
                    }
                    break;
            }   
            case 2:
                current_key = ID;
                break;
            case 3: {
                        switch (key[1]) {
                            case 'a':
                                current_key = LAT;
                                break;
                            case 'o':
                                current_key = LON;
                                break;
			    case 'e':
			    	current_key = REF;
				break;
                            case 'i':
				current_key = UID;
				break;
                            default:
                                current_key = UNKNOWN;
                                fprintf(stderr, "--> %c%c\n", key[0], key[1]);
                        }
                        break;
                    }
            case 4: {
	    		switch (key[0]) {
			    case 'u':
			    	current_key = USER;
				break;
			    case 'r':
			    	current_key = ROLE;
				break;
			    case 't':
			    	current_key = TYPE;
				break;
			    default:
			    	current_key = UNKNOWN;
				fprintf(stderr, "--> %c%c\n", key[0], key[1]);
			}
			break;
		}
	    case 7: {
		current_key = VERSION;
		break;
            }
            case 9: {
			switch (key[0]) {
				case 't':
					current_key = TIMESTAMP;
					break;
				case 'c':
					current_key = CHANGESET;
					break;
				default:
					current_key = UNKNOWN;
					fprintf(stderr, "--> %c%c\n", key[0], key[1]);
			}
                    break;
	    }
            default: {
                    char *thingie = strndup(key, (key_end - key));
                    current_key = UNKNOWN;
                    
                    fprintf(stderr, "UNKNOWN ATTR %s-> %c%c\n", thingie, key[0], key[1]);
                    free(thingie);
                }
        }

        value = key_end + 2;
	value_end = value;
	value_end = strchr(value_end, '"');

	if (value_end > end)
		break;

        switch (current_key) {
            case ID:
                if (attr_id) free(attr_id);
                attr_id = strndup(value, (value_end - value));
                break;

            case LAT:
                if (attr_lat) free(attr_lat);
                attr_lat = strndup(value, (value_end - value));
		attr_lat_uint = coordtouint(attr_lat);
                break;

            case LON:
                if (attr_lon) free(attr_lon);
                attr_lon = strndup(value, (value_end - value));
		attr_lon_uint = coordtouint(attr_lon);
                break;

            case TIMESTAMP:
                if (attr_timestamp) free(attr_timestamp);
//              attr_timestamp = strndup(value, (value_end - value));
                attr_timestamp = strndup(value, (value_end - (value + 1))); /* another stupid fix */
//		attr_timestamp[10] = ' '; /* Stupid timestamp fix */
                break;

	    case CHANGESET:
		if (attr_changeset) free(attr_changeset);
		attr_changeset = strndup(value, (value_end - value));
		break;

            case USER: {
	    	char *tmp;
                if (attr_user) free(attr_user);
                attr_user = strndup(value, (value_end - value));
		tmp = escape_string(attr_user);
		free(attr_user);
		attr_user = tmp;
                break;
	    }
            
	    case UID: {
                if (attr_uid) free(attr_uid);
                attr_uid = strndup(value, (value_end - value));
                break;
	    }

	    case VERSION: {
		if (attr_version) free(attr_version);
		attr_version = strndup(value, (value_end - value));
		break;
	    }

            case KEY: {
	    	char *tmp;
                if (attr_key) free(attr_key);
                attr_key = strndup(value, (value_end - value));
		tmp = escape_string(attr_key);
		free(attr_key);
		attr_key = tmp;
                break;
	    }
            
            case VALUE: {
	    	char *tmp;
                if (attr_value) free(attr_value);
                attr_value = strndup(value, (value_end - value));
		tmp = escape_string(attr_value);
		free(attr_value);
		attr_value = tmp;
                break;
	    }

            case TYPE:
                if (attr_type) free(attr_type);
                attr_type = strndup(value, (value_end - value));
                break;

            case REF:
                if (attr_ref) free(attr_ref);
                attr_ref = strndup(value, (value_end - value));
                break;

            case ROLE: {
	    	char *tmp;
                if (attr_role) free(attr_role);
		attr_role = strndup(value, (value_end - value));
		tmp = escape_string(attr_role);
		free(attr_role);
		attr_role = tmp;
                break;
	    }

            default:
                fprintf(stderr, "--> %c%c\n", value[0], value[1]);
        }

        key = value_end + 2;
    } while (key < end);

    if (ywk == DELETE || ywk == MODIFY) {
	FILE *this = NULL;
	switch (current_tag) {
		case NODE:
			if (ywk == DELETE) { this = fd_delete_nodes; count_delete_nodes++; } else { this = fd_modify_nodes; count_modify_nodes++; }
			break;
		case WAY:
			if (ywk == DELETE) { this = fd_delete_ways; count_delete_ways++; } else { this = fd_modify_ways; count_modify_ways++; }
			break;
		case RELATION:
			if (ywk == DELETE) { this = fd_delete_relations; count_delete_relations++; } else { this = fd_modify_relations; count_modify_relations++; }
			break;
	  }
	  if (this) {
		  fputs(attr_id, this);
		  fputs("\n", this);
	  }
    }

    if (ywk != DELETE) {
    switch (current_tag) {
        case NODE:
            fprintf(fd_nodes, "%s, %s, %s, %s, '%s', %lu\n", attr_id, attr_lon, attr_lat, (attr_uid != NULL ? attr_uid : "0"), attr_timestamp, zcurve(attr_lon, attr_lat));
#ifdef BENCHMARK
            fprintf(fd_nodes_uint, "%s, %d, %d, %s, '%s'\n", attr_id, attr_lon_uint, attr_lat_uint, (attr_uid != NULL ? attr_uid : "0"), attr_timestamp);
            fprintf(fd_nodes_gis, "%s, 'POINT( %s %s )', '%s', '%s'\n", attr_id, attr_lat, attr_lon, (attr_uid != NULL ? attr_uid : "0"), attr_timestamp);
#endif
	    count_nodes++;
            break;
        case TAG: {
		switch (parent_tag) {
			case NODE:
            		    fprintf(fd_node_tags, "%s, '%s', '%s'\n", attr_id, attr_key, attr_value);
		  	    count_node_tags++;
		            break;
			case WAY:
			    fprintf(fd_way_tags, "%s, '%s', '%s'\n", attr_id, attr_key, attr_value);
			    count_way_tags++;
			    break;
			case RELATION:
			    fprintf(fd_relation_tags, "%s, '%s', '%s'\n", attr_id, attr_key, attr_value);
			    count_relation_tags++;
			    break;
			default:
		 	    break;
		}
		break;
	}
        case WAY:
            fprintf(fd_ways, "%s, %s, '%s'\n", attr_id, (attr_uid != NULL ? attr_uid : "0"), attr_timestamp);
	    count_ways++;
//          fprintf(fd_way_tags, "%s, '%s', '%s'\n", attr_id, "type", "way");
//	    count_way_tags++;
            break;
        case RELATION:
            fprintf(fd_relations, "%s, %s, '%s'\n", attr_id, (attr_uid != NULL ? attr_uid : "0"), attr_timestamp);
	    count_relations++;
            break;
        case MEMBER:
                if (strcmp(attr_type, "node") == 0) {
                        fprintf(fd_members_node, "%s, %lu, %s, '%s'\n", attr_id, sequence, attr_ref, attr_role);
			count_members_node++;
		} else if (strcmp(attr_type, "way") == 0) {
                        fprintf(fd_members_way, "%s, %lu, %s, '%s'\n", attr_id, sequence, attr_ref, attr_role);
			count_members_way++;
		} else if (strcmp(attr_type, "relation") == 0) {
                        fprintf(fd_members_relation, "%s, %lu, %s, '%s'\n", attr_id, sequence, attr_ref, attr_role);
			count_members_relation++;
		}
		sequence++;
                break;
        case ND:
            fprintf(fd_way_nds, "%s, %lu, %s\n", attr_id, sequence, attr_ref);
	    sequence++;
	    count_way_nds++;
            break;
        default:
            break;
    }
    }

    if (end[-2] == '/') {
        switch (current_tag) {
            case NODE:
                free(attr_lat);
                free(attr_lon);
                attr_lat = NULL;
                attr_lon = NULL;
		attr_lat_uint = 0;
		attr_lon_uint = 0;
		/* no break! */

	    case WAY:
	    case RELATION:
                free(attr_id);
                free(attr_timestamp);
                free(attr_changeset);
                free(attr_user);
                free(attr_uid);
                free(attr_version);

                attr_id = attr_user = attr_uid = attr_timestamp = attr_changeset = attr_version = NULL;

	    	sequence = 0;
                break;

            case TAG:
                free(attr_key);
                free(attr_value);

                attr_key = NULL;
                attr_value = NULL;
                break;
	    
	    case ND:
	    case MEMBER:
	    	free(attr_type);
	    	free(attr_ref);
	    	free(attr_role);

		attr_type = NULL;
		attr_ref = NULL;
		attr_role = NULL;
	    default:
	    	break;
        }
    } else if (current_tag == NODE || current_tag == WAY || current_tag == RELATION) {
		parent_tag = current_tag;
	}

    
     nextline;  
    } while (stopcondition);
exit:
        
    free(attr_id);
    free(attr_lat);
    free(attr_lon);
    free(attr_timestamp);
    free(attr_changeset);
    free(attr_user);
    free(attr_uid);
    free(attr_version);

    free(attr_key);
    free(attr_value);

    fclose(fd_nodes);
#ifdef BENCHMARK
    fclose(fd_nodes_uint);
    fclose(fd_nodes_gis);
#endif
    fclose(fd_node_tags);
    fclose(fd_ways);
    fclose(fd_way_tags);
    fclose(fd_way_nds);
    fclose(fd_relations);
    fclose(fd_relation_tags);
    fclose(fd_members_node);
    fclose(fd_members_relation);
    fclose(fd_members_way);

    fclose(fd_delete_nodes);
    fclose(fd_delete_ways);
    fclose(fd_delete_relations);
    fclose(fd_modify_nodes);
    fclose(fd_modify_ways);
    fclose(fd_modify_relations);



    char *current = get_current_dir_name();

    puts("START TRANSACTION;");

    if (ywk == NONE) {
    puts("CREATE TABLE nodes_legacy (id integer, long double, lat double, uid integer, timestamp timestamptz, zcurve bigint);");
#ifdef BENCHMARK
    puts("CREATE TABLE nodes_legacy_uint (id integer, long integer, lat integer, uid integer, timestamp timestamptz);");
    puts("CREATE TABLE nodes_legacy_gis (id integer, poi point, uid integer, timestamp timestamptz);");
#endif
    puts("CREATE TABLE node_tags (node integer, k varchar(255), v varchar(1024));");
    puts("CREATE TABLE ways (id integer,uid integer, timestamp timestamptz);");
    puts("CREATE TABLE way_tags (way integer, k varchar(255), v varchar(1024));");
    puts("CREATE TABLE way_nds (way integer, idx integer, to_node integer);");
    puts("CREATE TABLE relations(id integer, uid integer, timestamp timestamptz);");
    puts("CREATE TABLE relation_members_node (relation integer, idx integer, to_node integer, role varchar(255));");
    puts("CREATE TABLE relation_members_relation (relation integer, idx integer, to_relation integer, role varchar(255));");
    puts("CREATE TABLE relation_members_way (relation integer, idx integer, to_way integer, role varchar(255));");
    puts("CREATE TABLE relation_tags (relation integer, k varchar(255), v varchar(1024));");
    } else {
      if (count_delete_nodes > 0 || count_delete_ways > 0 || count_delete_relations > 0 ||
          count_modify_nodes > 0 || count_modify_ways > 0 || count_modify_relations > 0) {
        if (count_delete_relations > 0 || count_modify_relations > 0) {
          puts("CREATE TEMPORARY TABLE delete_relations (id integer);");
        }
        if (count_delete_ways > 0 || count_modify_ways > 0) {
          puts("CREATE TEMPORARY TABLE delete_ways (id integer);");
        }
        if (count_delete_nodes > 0 || count_modify_nodes > 0) {
          puts("CREATE TEMPORARY TABLE delete_nodes (id integer);");
        }

        if (count_delete_nodes > 0 || count_delete_ways > 0 || count_delete_relations > 0) {
        if (count_delete_relations > 0) {
          printf("COPY %lu RECORDS INTO delete_relations FROM '%s/" file_delete_relations "' USING DELIMITERS ',', '\\n', '''';\n", count_delete_relations, current);
        }
        
        if (count_delete_ways > 0) {
          printf("COPY %lu RECORDS INTO delete_ways FROM '%s/" file_delete_ways "' USING DELIMITERS ',', '\\n', '''';\n", count_delete_ways, current);
        }

        if (count_delete_nodes > 0) {
          printf("COPY %lu RECORDS INTO delete_nodes FROM '%s/" file_delete_nodes "' USING DELIMITERS ',', '\\n', '''';\n", count_delete_nodes, current);
        }

        if (count_delete_relations > 0) {
	  puts("DELETE FROM relation_members_relation WHERE to_relation IN (SELECT id FROM delete_relations);");
        }
        
        if (count_delete_ways > 0) {         
          puts("DELETE FROM relation_members_way WHERE to_way IN (SELECT id FROM delete_ways);");
        }

        if (count_delete_nodes > 0) {
          puts("DELETE FROM relation_members_node WHERE to_node IN (SELECT id FROM delete_nodes);\n"
               "DELETE FROM way_nds WHERE to_node IN (SELECT id FROM delete_nodes);");
        }
        }

        if (count_modify_relations > 0) {
          printf("COPY %lu RECORDS INTO delete_relations FROM '%s/" file_modify_relations "' USING DELIMITERS ',', '\\n', '''';\n", count_modify_relations, current);
        }

        if (count_modify_ways > 0) {
          printf("COPY %lu RECORDS INTO delete_ways FROM '%s/" file_modify_ways "' USING DELIMITERS ',', '\\n', '''';\n", count_modify_ways, current);
        }
          
        if (count_modify_nodes > 0) {
          printf("COPY %lu RECORDS INTO delete_nodes FROM '%s/" file_modify_nodes "' USING DELIMITERS ',', '\\n', '''';\n", count_modify_nodes, current);
        }

        if (count_delete_relations > 0 || count_modify_relations > 0) {
          puts("DELETE FROM relation_members_relation WHERE relation IN (SELECT id FROM delete_relations);\n"
               "DELETE FROM relation_members_way WHERE relation IN (SELECT id FROM delete_relations);\n"
               "DELETE FROM relation_members_node WHERE relation IN (SELECT id FROM delete_relations);\n"
               "DELETE FROM relation_tags WHERE relation IN (SELECT id FROM delete_relations);\n"
               "DELETE FROM relations WHERE id IN (SELECT id FROM delete_relations);\n"
               "DROP TABLE delete_relations;\n");
        }
        
        if (count_delete_ways > 0 || count_modify_ways > 0) {
          puts("DELETE FROM way_nds WHERE way IN (SELECT id FROM delete_ways);\n"
               "DELETE FROM way_tags WHERE way IN (SELECT id FROM delete_ways);\n"
	       "DELETE FROM ways WHERE id IN (SELECT id FROM delete_ways);\n"
               "DROP TABLE delete_ways;\n");
        }

        if (count_delete_nodes > 0 || count_modify_nodes > 0) {
          puts("DELETE FROM node_tags WHERE node IN (SELECT id FROM delete_nodes);\n"
               "DELETE FROM nodes_legacy WHERE id IN (SELECT id FROM delete_nodes);\n"
               "DROP TABLE delete_nodes;\n");
        }
      }
    }
    
    printf("COPY %lu RECORDS INTO nodes_legacy from '%s/" file_nodes "' USING DELIMITERS ',', '\\n', '''';\n", count_nodes, current);
#ifdef BENCHMARK
    printf("COPY %lu RECORDS INTO nodes_legacy_uint from '%s/" file_nodes_uint "' USING DELIMITERS ',', '\\n', '''';\n", count_nodes, current);
    printf("COPY %lu RECORDS INTO nodes_legacy_gis from '%s/" file_nodes_gis "' USING DELIMITERS ',', '\\n', '''';\n", count_nodes, current);
#endif
    printf("COPY %lu RECORDS INTO node_tags from '%s/" file_node_tags "' USING DELIMITERS ',', '\\n', '''';\n", count_node_tags, current);
    printf("COPY %lu RECORDS INTO ways from '%s/" file_ways "' USING DELIMITERS ',', '\\n', '''';\n", count_ways, current);
    printf("COPY %lu RECORDS INTO way_tags from '%s/" file_way_tags "' USING DELIMITERS ',', '\\n', '''';\n", count_way_tags, current);
    printf("COPY %lu RECORDS INTO way_nds from '%s/" file_way_nds "' USING DELIMITERS ',', '\\n', '''';\n", count_way_nds, current);
    printf("COPY %lu RECORDS INTO relations from '%s/" file_relations "' USING DELIMITERS ',', '\\n', '''';\n", count_relations, current);
    printf("COPY %lu RECORDS INTO relation_tags from '%s/" file_relation_tags "' USING DELIMITERS ',', '\\n', '''';\n", count_relation_tags, current);
    printf("COPY %lu RECORDS INTO relation_members_node from '%s/" file_relation_member_node "' USING DELIMITERS ',', '\\n', '''';\n", count_members_node, current);
    printf("COPY %lu RECORDS INTO relation_members_relation from '%s/" file_relation_member_relation "' USING DELIMITERS ',', '\\n', '''';\n", count_members_relation, current);
    printf("COPY %lu RECORDS INTO relation_members_way from '%s/" file_relation_member_way "' USING DELIMITERS ',', '\\n', '''';\n", count_members_way, current);

    free(current);

    puts("COMMIT;");

    if (ywk == NONE) {
    puts("START TRANSACTION;");
    
    puts("CREATE SEQUENCE s_nodes AS INTEGER;");
    puts("ALTER SEQUENCE s_nodes RESTART WITH (SELECT MAX(id) FROM nodes_legacy);");
    puts("ALTER TABLE nodes_legacy ALTER COLUMN id SET NOT NULL;");
    puts("ALTER TABLE nodes_legacy ALTER COLUMN id SET DEFAULT NEXT VALUE FOR \"sys\".\"s_nodes\";");
    puts("ALTER TABLE nodes_legacy ADD CONSTRAINT pk_nodes_id PRIMARY KEY (id);");

    puts("CREATE SEQUENCE s_ways AS INTEGER;");
    puts("ALTER SEQUENCE s_ways RESTART WITH (SELECT MAX(id) FROM ways);");
    puts("ALTER TABLE ways ALTER COLUMN id SET NOT NULL;");
    puts("ALTER TABLE ways ALTER COLUMN id SET DEFAULT NEXT VALUE FOR \"sys\".\"s_ways\";");
    puts("ALTER TABLE ways ADD CONSTRAINT pk_ways_id PRIMARY KEY (id);");

    puts("CREATE SEQUENCE s_relations AS INTEGER;");
    puts("ALTER SEQUENCE s_relations RESTART WITH (SELECT MAX(id) FROM relations);");
    puts("ALTER TABLE relations ALTER COLUMN id SET NOT NULL;");
    puts("ALTER TABLE relations ALTER COLUMN id SET DEFAULT NEXT VALUE FOR \"sys\".\"s_relations\";");
    puts("ALTER TABLE relations ADD CONSTRAINT pk_relations_id PRIMARY KEY (id);");

    puts("ALTER TABLE relation_members_node ADD CONSTRAINT pk_relation_members_node PRIMARY KEY (relation, idx);");
    puts("ALTER TABLE relation_members_way ADD CONSTRAINT pk_relation_members_way PRIMARY KEY (relation,idx);");
    puts("ALTER TABLE relation_members_relation ADD CONSTRAINT pk_relation_members_relation PRIMARY KEY (relation,idx);");

    puts("COMMIT;");


    puts("START TRANSACTION;");
    
    puts("ALTER TABLE node_tags ADD CONSTRAINT fk_node_tags_node FOREIGN KEY (node) REFERENCES nodes_legacy (id);");
    puts("ALTER TABLE node_tags ADD CONSTRAINT pk_node_tags UNIQUE (node, k, v);");

    puts("ALTER TABLE way_tags ADD CONSTRAINT fk_way_tags_way FOREIGN KEY (way) REFERENCES ways (id);");
    puts("ALTER TABLE way_tags ADD CONSTRAINT pk_way_tags UNIQUE (way, k, v);");

    puts("ALTER TABLE way_nds ADD CONSTRAINT pk_way_nds PRIMARY KEY (way, idx);");
    puts("ALTER TABLE way_nds ADD CONSTRAINT fk_way_nds_way FOREIGN KEY (way) REFERENCES ways (id);");
    puts("ALTER TABLE way_nds ADD CONSTRAINT fk_way_nds_node FOREIGN KEY (to_node) REFERENCES nodes_legacy (id);");

    puts("ALTER TABLE relation_tags ADD CONSTRAINT fk_relation_tags FOREIGN KEY (relation) REFERENCES relations (id);");
    puts("ALTER TABLE relation_tags ADD CONSTRAINT pk_relation_tags UNIQUE (relation, k, v);");
    
    puts("ALTER TABLE relation_members_node ADD CONSTRAINT fk_relation_members_node FOREIGN KEY (relation) REFERENCES relations (id);");
    puts("ALTER TABLE relation_members_node ADD CONSTRAINT fk_relation_members_tonode FOREIGN KEY (to_node) REFERENCES nodes_legacy (id);");

    puts("ALTER TABLE relation_members_way ADD CONSTRAINT fk_relation_members_way FOREIGN KEY (relation) REFERENCES relations (id);");
    puts("ALTER TABLE relation_members_way ADD CONSTRAINT fk_relation_members_toway FOREIGN KEY (to_way) REFERENCES ways (id);");

    puts("ALTER TABLE relation_members_relation ADD CONSTRAINT fk_relation_members_relation FOREIGN KEY (relation) REFERENCES relations (id);");
    puts("ALTER TABLE relation_members_relation ADD CONSTRAINT fk_relation_members_torelation FOREIGN KEY (to_relation) REFERENCES relations (id);");

    puts("COMMIT;");
    }
}


int main(int argc, char *argv[]) {
#ifdef MMAP
    int fd;
    struct stat statbuf;

    if (argc != 2)
        exit(-1);

    fprintf(stderr, "Analysing %s...\n", argv[1]);

    fd = open(argv[1], O_RDONLY);

    if (fd < 0)
        exit(-1);

    if (fstat (fd, &statbuf) == -1) { perror("fstat:"); exit(-1); }

    if (statbuf.st_size > 0) {
        char *range = NULL;
        range = mmap(NULL, statbuf.st_size, PROT_READ, MAP_SHARED, fd, (off_t) 0);
        if (range == MAP_FAILED) { perror("Mmap:"); puts("(did you compile PAE in the kernel?)"); exit(-1); }
        parser(range, statbuf.st_size / sizeof(char));
        munmap(range, statbuf.st_size);
    }

    close(fd);
#else
    parser();
#endif
    exit(0);
}
