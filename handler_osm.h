/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* Cherokee
 *
 * Authors:
 *      Alvaro Lopez Ortega <alvaro@alobbs.com>
 *
 * Copyright (C) 2001-2008 Alvaro Lopez Ortega
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CHEROKEE_HANDLER_OSM_H
#define CHEROKEE_HANDLER_OSM_H

#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include <Mapi.h>

#define LEGACY_SQL 1

/* Data types
 */

typedef struct {
	CHEROKEE_RWLOCK_T(lock);
	int dummy;
} cherokee_handler_osm_priv_t;

#define OSM_PRIV(x) ((cherokee_handler_osm_priv_t *)((x)->priv))
#define OSM_LOCK(x) (&OSM_PRIV(x)->lock)

typedef struct {
	cherokee_module_props_t base;
	cherokee_boolean_t	read_only;
	cherokee_buffer_t	db_hostname;
	cint_t			db_port;
	cherokee_buffer_t	db_username;
	cherokee_buffer_t	db_password;
	cherokee_buffer_t	db_name;

	void * priv;
	cherokee_list_t		dbh_pool;
} cherokee_handler_osm_props_t;

typedef struct {
	Mapi dbh;
	cherokee_handler_t       handler;
	cherokee_buffer_t        buffer;
} cherokee_handler_osm_t;


#define HDL_OSM(x)       ((cherokee_handler_osm_t *)(x))
#define PROP_OSM(x)      ((cherokee_handler_osm_props_t *)(x))
#define HDL_OSM_PROPS(x) (PROP_OSM(MODULE(x)->props))


/* Library init function
 */
void  PLUGIN_INIT_NAME(osm)      (cherokee_plugin_loader_t *loader);
ret_t cherokee_handler_osm_new   (cherokee_handler_t **hdl, cherokee_connection_t *cnt, cherokee_module_props_t *props);

/* virtual methods implementation
 */
ret_t cherokee_handler_osm_init        (cherokee_handler_osm_t *hdl);
ret_t cherokee_handler_osm_free        (cherokee_handler_osm_t *hdl);
ret_t cherokee_handler_osm_step        (cherokee_handler_osm_t *hdl, cherokee_buffer_t *buffer);
ret_t cherokee_handler_osm_add_headers (cherokee_handler_osm_t *hdl, cherokee_buffer_t *buffer);

#endif /* CHEROKEE_HANDLER_OSM_H */
