#include <cherokee/common-internal.h>
#include <cherokee/cherokee.h>
#include <Mapi.h>

#include "handler_osm.h"
#include "handler_osm_db.h"
#include "handler_osm_delete.h"
#include "handler_osm_sql.h"

ret_t
cherokee_handler_osm_init_delete (cherokee_handler_osm_t *hdl) {
	ret_t ret = ret_error;
	osm_state_delete_t state = OSM_DELETE_FIRST;
	cherokee_connection_t *conn = HANDLER_CONN(hdl);
	char *string = conn->request.buf;
	char *token;

	while (state != OSM_DELETE_DONE && (token = (char *) strsep( &string , "/")) != NULL) {
		if (*token == '\0')
			continue;

		switch (state) {
			case OSM_DELETE_FIRST:
				switch (token[0]) {
					case 'n':
						state = OSM_DELETE_NODE_ID;
						break;
					case 'w':
						state = OSM_DELETE_WAY_ID;
						break;
					case 'r':
						state = OSM_DELETE_RELATION_ID;
						break;
				}
				break;

			case OSM_DELETE_NODE_ID:
			case OSM_DELETE_WAY_ID:
			case OSM_DELETE_RELATION_ID: {
				unsigned long id = 0;
				id = strtoul(token, (char **) NULL, 10);
				if (errno != ERANGE) {
					state++;
					ret = delete_object_by_id(hdl, id, state);
				} else 
					state = OSM_DELETE_DONE;
				break;
			}
			default:
				ret = ret_error;
		}
	}

	return ret;
}

ret_t
delete_object_by_id(cherokee_handler_osm_t *hdl, unsigned long int id, osm_state_delete_t state) {
	ret_t ret;
	cherokee_buffer_t sql = CHEROKEE_BUF_INIT;
	switch (state) {
		case OSM_DELETE_NODE_COMMAND:
			cherokee_buffer_add_va (&sql, SQL_DELETE_NODE_BY_ID, id);
			ret = run_sql(hdl, &sql, NULL, NULL);
			break;

		case OSM_DELETE_WAY_COMMAND:
			cherokee_buffer_add_va (&sql, SQL_DELETE_WAY_BY_ID, id);
			ret = run_sql(hdl, &sql, NULL, NULL);
			break;

		case OSM_DELETE_RELATION_COMMAND:
			cherokee_buffer_add_va (&sql, SQL_DELETE_RELATION_BY_ID, id);
			ret = run_sql(hdl, &sql, NULL, NULL);			
			break;
		default:
			ret = ret_error;
	}
	cherokee_buffer_mrproper(&sql);
	return ret;
}


